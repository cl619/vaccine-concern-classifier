from setuptools import setup, find_packages

setup(
    name='vax_concerns_classifiers',
    version='0.1',
    packages=find_packages(),
)