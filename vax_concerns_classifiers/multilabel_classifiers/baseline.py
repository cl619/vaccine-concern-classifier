from .._vax_concerns_utils import *

class MultilabelBaseline(VaxConcernsBaseClassifier):
    '''
    Baseline multilabel Bert classifier
    '''
    def __init__(self) -> None:
        '''
        Initializes the baseline multilabel classifier
        '''
        # model parameters
        self.bert_model_name = 'bert-base-uncased'
        self.num_classes = len(CATEGORIES)
        self.max_length = 128
        self.batch_size = 16
        self.num_epochs = 2
        self.learning_rate = 2e-5
        local_dir = os.path.dirname(os.path.abspath(__file__))
        self.tokenizer = BertTokenizer.from_pretrained(os.path.join(local_dir, 'bert_tokenizer'))
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        if torch.cuda.is_available():
            print(f"Using GPU: {torch.cuda.get_device_name(0)}")
        else:
            print("Using CPU")
        self.model = BERTClassifier(self.bert_model_name, self.num_classes).to(self.device)
        self.ort_session = None
        self.loss_function = nn.BCEWithLogitsLoss()
        self.tuning_precision = 3

        # model variables for saving
        self.save_freq = 25
        self.history = {'train': {'logits': {}, 'labels': {}},
                         'tuning': {'logits': {}, 'labels': None},
                         'val': {'logits': {}, 'labels': None},
                         'best_cutoffs': {},
                         'seed': torch.seed()}
        self.global_step = -1

    def __str__(self) -> str:
        '''
        Returns a string version of the classifier object
        '''
        return str(torchinfo.summary(self.model.get_bert()))

    def _hyperparams_str(self) -> str:
        return f"\nRandom seed: {self.history['seed']}"

    def fit(self, 
            train_data: tuple[list[str], list[int]], 
            tuning_data: tuple[list[str], list[int]], 
            val_data: tuple[list[str], list[int]], 
            ) -> None:
        '''
        Fits the model to the training data, tunes cutoffs with tuning, and returns validation data
        
        Parameters
        -------------
        train_data : tuple[list[str], list[int]]
            training data, tuple of text data and labels
        tuning_data : tuple[list[str], list[int]]
            cutoff tuning data, tuple of text data and labels
        val_data : tuple[list[str], list[int]]
            validation data, tuple of text data and labels
        '''
        print("TRAINING BASELINE")
        # initialize dataset objects
        train_dataset = TextClassificationDataset(self.tokenizer, self.max_length, train_data[0], train_data[1])
        val_dataset = TextClassificationDataset(self.tokenizer, self.max_length, val_data[0], val_data[1])
        tuning_dataset = TextClassificationDataset(self.tokenizer, self.max_length, tuning_data[0], tuning_data[1])

        # initialize dataloaders
        self.train_dataloader = DataLoader(train_dataset, batch_size=self.batch_size, shuffle=True)
        self.val_dataloader = DataLoader(val_dataset, batch_size=self.batch_size)
        self.tuning_dataloader = DataLoader(tuning_dataset, batch_size=self.batch_size)

        optimizer = torch.optim.AdamW(self.model.parameters(), lr=self.learning_rate)
        total_steps = len(self.train_dataloader) * self.num_epochs
        scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps=0, num_training_steps=total_steps)

        for epoch in range(self.num_epochs):
            print(f"EPOCH {epoch + 1}/{self.num_epochs}")
            self._train(self.train_dataloader, optimizer, scheduler)
            train_labels = self.history['train']['labels'][self.global_step]
            train_logits = self.history['train']['logits'][self.global_step]
            print(f"TRAINING LOSS: {self.loss_function(train_logits, train_labels)}")

            # print best cutoffs found
            print("\tTUNING:")
            for cat, bc in zip(CATEGORIES, self.history['best_cutoffs'][self.global_step][0]):
                print(f"\t\tVax concern {cat} cutoff: {bc}")
            tuning_labels = np.array(self.history['tuning']['labels'])
            tuning_logits = self.history['tuning']['logits'][self.global_step]
            tuning_probabilities = np.array(self.logits_to_probabilities(tuning_logits))
            tuning_predictions = self._probabilities_to_binary(tuning_probabilities, self.history['best_cutoffs'][self.global_step])
            print_report(tuning_labels, tuning_predictions)

            # evaluate model using the validation dataset and the best cutoffs found
            print("\tVALIDATION:")
            val_labels = np.array(self.history['val']['labels'])
            val_logits = self.history['val']['logits'][self.global_step]
            val_probabilities = np.array(self.logits_to_probabilities(val_logits))
            val_predictions = self._probabilities_to_binary(val_probabilities, self.history['best_cutoffs'][self.global_step])
            print_report(val_labels, val_predictions)
            print("\n" * 3)

        print(self._hyperparams_str())

    def _probabilities_to_binary(self, probabilities: ArrayLike, cutoff_s: ArrayLike) -> ArrayLike:
        '''
        Returns binary classification given the cutoff(s).
        Automatically broadcasts if there are distinct cutoffs for each class

        Parameters
        -----------------
        probabilities : ArrayLike
            the probability predictions with shape (num_samples, num_classes)
        cutoff_s : ArrayLike
            the cutoff(s), with shape (1, num_samples)
        '''
        return np.where(probabilities < cutoff_s, 0.0, 1.0)

    def _calculate_cutoffs(self, labels: ArrayLike, probabilities: ArrayLike, precision: int) -> ArrayLike:
        '''
        Returns the best cutoffs

        Parameters
        -----------------
        labels : ArrayLike
            a list of multi-hot encoded labels with shape (num_samples, num_classes)
        probabilities : ArrayLike
            probability logits with shape (num_samples, num_classes)
        precision : int
            order of magnitude of precision desired
        '''
        factor = 10.0 ** precision
        inv_factor = 1.0 / factor
        range_min = np.floor(np.min(probabilities) * factor) * inv_factor
        range_max = np.ceil(np.max(probabilities) * factor) * inv_factor
        cur_best_cutoffs = np.zeros((1, len(CATEGORIES)))
        cutoffs = np.arange(range_min, range_max + inv_factor, inv_factor)
        for i in range(len(CATEGORIES)):
            f1_scores = []
            for c in cutoffs:
                predictions = self._probabilities_to_binary(probabilities, c)
                f1_scores.append(f1_score(labels[:, i], predictions[:, i], average='micro', zero_division=0.0))
            cur_best_cutoffs[0, i] = np.round(cutoffs[np.argmax(f1_scores)], precision)
        return cur_best_cutoffs

    def _train(self, 
               data_loader: DataLoader, 
               optimizer: Optimizer, 
               scheduler: LambdaLR,
               ) -> None:
        '''
        Internal train method
        
        Parameters
        --------------
        data_loader : DataLoader
            DataLoader object to batch data
        optimizer : Optimizer, 
            Optimizer object for model training
        scheduler : LambdaLR
            Scheduler object for model training
        '''
        epoch_num_steps = len(data_loader)
        for batch in tqdm(data_loader):
            self.model.train()
            self.global_step += 1
            optimizer.zero_grad()
            input_ids = batch['input_ids'].to(self.device)
            attention_mask = batch['attention_mask'].to(self.device)
            labels = batch['label'].to(self.device)

            logits = self.model(input_ids=input_ids, attention_mask=attention_mask)

            loss = self.loss_function(logits, labels) # had to switch regular order here
            loss.backward()
            optimizer.step()
            scheduler.step()

            # save training labels and logits every step
            self.history['train']['labels'][self.global_step] = labels.detach().cpu()
            self.history['train']['logits'][self.global_step] = logits.detach().cpu()
            # save everything else every self.save_freq (25 steps)
            if ((self.global_step % self.save_freq) == 0 or \
                    (self.global_step + 1) % epoch_num_steps == 0) and \
                    self.global_step not in self.history['best_cutoffs']:
                # store logits on tuning dataset
                tuning_labels, tuning_logits = self.eval_logits(self.tuning_dataloader)
                if self.history['tuning']['labels'] is None:
                    self.history['tuning']['labels'] = tuning_labels
                self.history['tuning']['logits'][self.global_step] = tuning_logits
                # store best_cutoffs calculated from tuning dataset
                tuning_probabilities = self.logits_to_probabilities(tuning_logits)
                self.history['best_cutoffs'][self.global_step] = self._calculate_cutoffs(np.array(tuning_labels), 
                                                                                          np.array(tuning_probabilities), 
                                                                                          self.tuning_precision)
                # store logits on validation dataaset
                val_labels, val_logits = self.eval_logits(self.val_dataloader)
                if self.history['val']['labels'] is None:
                    self.history['val']['labels'] = val_labels
                self.history['val']['logits'][self.global_step] = val_logits
    
    def save_model(self, dir_path: str, name: str) -> None:
        '''
        Saves the model and history to files {name}_history.pkl and {name}_classifier.pth

        Parameters
        -----------------
        dir_path : str
            directory path to save files
        name : str
            name of the classifier (affects the file name)
        '''
        folder = os.path.join(dir_path, name)
        count = 1
        while os.path.exists(folder):
            folder = os.path.join(dir_path, name + '_' + str(count))
            count += 1
        os.mkdir(folder)

        self.history['global_step'] = self.global_step
        with open(os.path.join(folder, name + "_history.pkl"), 'wb') as f:
            pickle.dump(self.history, f)
        torch.save(self.model.state_dict(), os.path.join(folder, name + "_classifier.pth"))

    def load_model(self, dir_path: str, name: str) -> None:
        '''
        Loads the model from history and classifier files

        Parameters
        -----------------
        dir_path : str
            directory path to load files
        name : str
            name of the classifier (affects the file name)
        '''
        with open(os.path.join(dir_path, name + "_history.pkl"), 'rb') as f:
            self.history = pickle.load(f)
        self.global_step = self.history['global_step']
        self.model.load_state_dict(torch.load(os.path.join(dir_path, name + "_classifier.pth")))

        # # Create session options
        # session_options = ort.SessionOptions()
        # session_options.intra_op_num_threads = 1  # Restrict intra-op parallelism
        # session_options.execution_mode = ort.ExecutionMode.ORT_SEQUENTIAL  # Avoid parallel execution issues
        # session_options.enable_cpu_mem_arena = False  # Disable CPU memory arena
        # session_options.enable_mem_pattern = False  # Disable memory pattern optimizations
        # self.ort_session = ort.InferenceSession(f"{dir_path}/{name}.onnx", providers=['CUDAExecutionProvider' if torch.cuda.is_available() else 'CPUExecutionProvider'])

    def get_history(self) -> dict:
        '''
        Returns the model training history
        '''
        return self.history
    
    def plot_threshold_analysis(self, type_s: list[str], step: int=None) -> None:
        '''
        Plots precision recall f1 graphs and roc curves every 25 steps in the model training

        Parameters
        --------------------
        type_s : list[str]
            A list of the 5x5 plots, can be 'prf' or 'roc
        step : int
            The step to plot the threshold analysis graphs, default is the last step of training
        '''
        step = self.global_step if step is None else step
        tuning_labels = self.history['tuning']['labels']
        tuning_logits = self.history['tuning']['logits'][step]
        tuning_probabilities = self.logits_to_probabilities(tuning_logits) 
        best_cutoffs = self.history['best_cutoffs'][step]
        cutoffs = np.linspace(0, 1, 501)
        tuning_all_binary_predictions = [self._probabilities_to_binary(tuning_probabilities, c) for c in cutoffs]
        
        num_rows = len(type_s)
        fig = plt.figure(figsize=(25, 25 * num_rows))

        outer_gs = gridspec.GridSpec(num_rows, 1, figure=fig)
        for row, tp in enumerate(type_s):
            inner_gs = gridspec.GridSpecFromSubplotSpec(5, 5, subplot_spec=outer_gs[row])
            inner_axes = []
            for i in range(5):
                for j in range(5):
                    ax = fig.add_subplot(inner_gs[i, j])
                    inner_axes.append(ax)
            
            if tp == "prf":
                recall_scores = [recall_score(tuning_labels, tuning_all_binary_predictions[j], average='macro', zero_division=1.0) for j in range(501)]
                precision_scores = [precision_score(tuning_labels, tuning_all_binary_predictions[j], average='macro', zero_division=1.0) for j in range(501)]
                f1_scores = [f1_score(tuning_labels, tuning_all_binary_predictions[j], average='macro', zero_division=1.0) for j in range(501)]
                curves = zip([precision_scores, recall_scores, f1_scores], ['precision', 'recall', 'f1'])
                [inner_axes[-1].plot(cutoffs, scores, label=label) for scores, label in curves]
                inner_axes[-1].set_title('average')

                def _plot_prf(col, ax):
                    precision_scores = [precision_score(tuning_labels[:,col], tuning_all_binary_predictions[j][:,col], zero_division=1.0) for j in range(501)]
                    recall_scores = [recall_score(tuning_labels[:,col], tuning_all_binary_predictions[j][:,col], zero_division=1.0) for j in range(501)]
                    f1_scores = [f1_score(tuning_labels[:,col], tuning_all_binary_predictions[j][:,col], zero_division=1.0) for j in range(501)]
                    curves = zip([precision_scores, recall_scores, f1_scores], ['precision', 'recall', 'f1'])
                    [ax.plot(cutoffs, scores, label=label) for scores, label in curves]
                plot_func = _plot_prf
            if tp == "roc":
                tprs = []
                fprs = []
                for j in range(501):
                    tpr_row = []
                    fpr_row = []
                    for col in range(24):
                        tn, fp, fn, tp = confusion_matrix(tuning_labels[:,col], tuning_all_binary_predictions[j][:,col]).ravel()
                        tpr_row.append(tp / (tp + fn))
                        fpr_row.append(fp / (fp + tn))
                    tprs.append(tpr_row)
                    fprs.append(fpr_row)
                fpr = np.mean(np.array(fprs).T, axis=0)
                tpr = np.mean(np.array(tprs).T, axis=0)
                roc_auc = metrics.auc(fpr, tpr)
                display = metrics.RocCurveDisplay(fpr=fpr, tpr=tpr, roc_auc=roc_auc, estimator_name='baseline')
                display.plot(ax=inner_axes[-1])
                inner_axes[-1].set_title('average')

                def _plot_roc(col, ax):
                    fpr, tpr, thresholds = metrics.roc_curve(tuning_labels[:,col], tuning_probabilities[:,col])
                    roc_auc = metrics.auc(fpr, tpr)
                    display = metrics.RocCurveDisplay(fpr=fpr, tpr=tpr, roc_auc=roc_auc, estimator_name='baseline')
                    display.plot(ax=ax)
                plot_func = _plot_roc
            
            for i, (ax, cat) in enumerate(zip(inner_axes[:-1], CATEGORIES)):
                plot_func(i, ax)
                ax.set_title(f'{cat}')
                ax.legend(loc='lower center')
            
        plt.tight_layout()
        plt.show()

    def _get_last_index(self, lst: set[int], x: int) -> int:
        '''
        Helper function to get the maximum element in lst <= x, in other words lower x to the closest value in lst
        
        Parameters
        --------------------
        lst : set[int]
            list of integers
        x : int
            integer value
        '''
        return max([e for e in lst if e <= x])

    def _rolling_average(self, lst: list[float], window_size: int=25) -> list[int]:
        '''
        Given a list of floats, returns a rolling average of the values with padding
        
        Parameters
        -------------------
        lst : list[float]
            list of float values
        window_size : int
            window size of the rolling average
        '''
        length = len(lst)
        ret = [None] * length
        lst = [lst[0]] * (window_size // 2) + lst + [lst[-1]] * (window_size // 2)
        for i in range(length):
            ret[i] = sum(lst[i : i + window_size]) / window_size
        return ret

    def plot_history(self, type_s: list[str]) -> None:
        '''
        Plots metrics over time, loss or f1

        Parameters
        --------------------
        type_s : list[str]
            A list of what metrics to plot, 'loss' or 'f1'
        step : int
            The step to plot the history graphs, default is the last step of training
        '''
        num_plots = len(type_s)
        num_cols = 2
        num_rows = (num_plots // num_cols) + (num_plots % num_cols)
        fig, axes = plt.subplots(num_rows, num_cols, figsize=(6 * num_cols, 5 * num_rows))
        axes = axes.flatten()
        
        train_logits = self.history['train']['logits']
        train_labels = self.history['train']['labels']
        val_logits = self.history['val']['logits']
        val_labels = self.history['val']['labels']
        best_cutoffs = self.history['best_cutoffs']

        for ax, tp in zip(axes, type_s):
            if tp == 'loss':
                metric = lambda labels, logits, bc: self.loss_function(logits, labels)
            elif tp == 'f1':
                def _logits_to_f1(labels, logits, bc):
                    labels = np.array(labels)
                    logits = np.array(torch.sigmoid(logits))
                    binary_predictions = self._probabilities_to_binary(logits, bc)
                    return f1_score(labels, binary_predictions, average='micro', zero_division=0.0)
                metric = _logits_to_f1
            else: 
                continue
            
            train_times = range(self.history['global_step'] + 1)
            train_metric = [metric(train_labels[tm], train_logits[tm], best_cutoffs[self._get_last_index(best_cutoffs.keys(), tm)]) for tm in train_times]
            train_metric = self._rolling_average(train_metric)
            val_times = sorted(list(self.history['best_cutoffs'].keys()))
            val_metric = [metric(val_labels, val_logits[tm], best_cutoffs[tm]) for tm in val_times]

            ax.plot(train_times, train_metric, label='train', linewidth=1, alpha=0.8)
            ax.plot(val_times, val_metric, label='validation')
            ax.set_xlabel('step (batch size 16)')
            ax.set_ylabel(tp)
            ax.legend()
            ax.set_title(f'Training/Validation {tp.capitalize()}')

        plt.tight_layout()
        plt.show()

    def eval_logits(self, data_loader: DataLoader) -> tuple[torch.Tensor, torch.Tensor]:
        '''
        Given a data_loader object with labels, returns labels and logits

        Parameters
        ---------------------
        data_loader : DataLoader
            DataLoader object to batch data
        '''
        self.model.eval()
        labels = []
        logits = []
        with torch.no_grad():
            for batch in tqdm(data_loader, leave=False, file=sys.stderr):
                input_ids = batch['input_ids'].to(self.device)
                attention_mask = batch['attention_mask'].to(self.device)
                lb = batch['label'].to(self.device)

                lg = self.model(input_ids=input_ids, attention_mask=attention_mask)

                labels.append(lb.cpu())
                logits.append(lg.cpu())
        labels = torch.cat(labels, dim=0)
        logits = torch.cat(logits, dim=0)
        return labels, logits

    def logits_to_probabilities(self, logits: torch.Tensor) -> torch.Tensor:
        return torch.sigmoid(logits)

    def evaluate(self, data_loader: DataLoader) -> tuple[ArrayLike, ArrayLike]:
        '''
        Given a data_loader object with labels, returns labels and probabilities

        Parameters
        ---------------------
        data_loader : DataLoader
            DataLoader object to batch data
        '''
        labels, logits = self.eval_logits(data_loader)
        probabilities = self.logits_to_probabilities(logits)
        return np.array(labels), np.array(probabilities)
    
    def predict(self, test_data: list[str]) -> ArrayLike:
        '''
        Returns binary labels given test data

        Parameters:
        ------------------
        test_data : List[str]
            input data
        '''
        test_dataset = TextClassificationDataset(self.tokenizer, self.max_length, test_data)
        test_data_loader = DataLoader(test_dataset, batch_size=self.batch_size)
        
        self.model.eval()
        logits = []
        with torch.no_grad():
            for batch in tqdm(test_data_loader):
                input_ids = batch['input_ids'].to(self.device)
                attention_mask = batch['attention_mask'].to(self.device)

                lg = self.model(input_ids=input_ids, attention_mask=attention_mask)
                logits.append(lg.cpu())
            logits = torch.cat(logits, dim=0)
            probabilities = self.logits_to_probabilities(logits)
        return self._probabilities_to_binary(np.array(probabilities), self.history['best_cutoffs'][self.global_step])

    # def predict_onnx(self, test_data: list[str]) -> ArrayLike:
    #     '''
    #     Returns binary labels given test data

    #     Parameters:
    #     ------------------
    #     test_data : List[str]
    #         input data
    #     '''
    #     test_dataset = TextClassificationDataset(self.tokenizer, self.max_length, test_data)
    #     test_data_loader = DataLoader(test_dataset, batch_size=self.batch_size)
        
    #     logits = []
    #     for batch in tqdm(test_data_loader):
    #         input_ids = batch['input_ids'].numpy()
    #         attention_mask = batch['attention_mask'].numpy()

    #         # ONNX Inference
    #         ort_inputs = {"input_ids": input_ids, "attention_mask": attention_mask}
    #         ort_outs = self.ort_session.run(None, ort_inputs)
    #         logits.append(ort_outs[0])
    #     logits = np.concatenate(logits, axis=0)
    #     probabilities = self.logits_to_probabilities(torch.from_numpy(logits))
    #     return self._probabilities_to_binary(np.array(probabilities), self.history['best_cutoffs'][self.global_step])