from .._vax_concerns_utils import *
from .baseline import MultilabelBaseline

class MultilabelWeighted(MultilabelBaseline):
    '''
    Multilabel classifier that inherits most methods from MultilabelBaseline,
    except that in _train, BCE loss is calculated to account for the uneven
    positive/negative distribution for each class
    '''

    def _hyperparams_str(self) -> str:
        return super()._hyperparams_str() + f"\nClass imbalance weights:\n{str(self.pos_weight)}"

    def fit(self, 
            train_data: tuple[list[str], list[int]], 
            tuning_data: tuple[list[str], list[int]], 
            val_data: tuple[list[str], list[int]], 
            weight_func,
            ) -> None:
        '''
        Fits the model to the training data, tunes cutoffs with tuning, and returns validation data.
        Follows the same behavior as baseline, except the loss accounts for each class distribution.
        
        Note: this assumes that each class has at least 1 positive example
        
        Parameters
        -------------
        train_data : tuple[list[str], list[int]]
            training data, tuple of text data and labels
        tuning_data : tuple[list[str], list[int]]
            cutoff tuning data, tuple of text data and labels
        val_data : tuple[list[str], list[int]]
            validation data, tuple of text data and labels
        tuning_precision : int = 1000
            specifies precision, default is to the thousandth
        '''
        multi_hot_labels = np.array(train_data[1])
        num_pos_samples = np.sum(multi_hot_labels, axis=0)
        num_neg_samples = multi_hot_labels.shape[0] - num_pos_samples
        self.pos_weight = torch.tensor(num_neg_samples / num_pos_samples)
        self.pos_weight = weight_func(self.pos_weight)
        self.loss_function = nn.BCEWithLogitsLoss(pos_weight=self.pos_weight)

        super().fit(train_data, tuning_data, val_data)