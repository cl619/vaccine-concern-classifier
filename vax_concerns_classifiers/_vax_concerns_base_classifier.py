class VaxConcernsBaseClassifier:
    def __init__(self):
        '''
        initialize classifier
        '''
        raise NotImplementedError

    def __str__(self):
        '''
        Gets the string representation of classifier
        '''
        raise NotImplementedError

    def fit(self):
        '''
        Trains the classifier
        '''
        raise NotImplementedError

    def predict(self):
        '''
        Uses the classifier to make predictions
        '''
        raise NotImplementedError