from .._vax_concerns_utils import *

class RelevanceBaseline(VaxConcernsBaseClassifier):
    '''
    Baseline relevance Bert classifier
    '''
    def __init__(self) -> None:
        '''
        Initializes the baseline relevance classifier
        '''
        self.bert_model_name = 'bert-base-uncased'
        self.num_classes = 2
        self.max_length = 128
        self.batch_size = 16
        self.num_epochs = 2
        self.learning_rate = 2e-5
        self.tokenizer = BertTokenizer.from_pretrained(self.bert_model_name)
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.model = BERTClassifier(self.bert_model_name, self.num_classes).to(self.device)
        self.loss_function = nn.CrossEntropyLoss()

        # model variables for saving
        self.save_freq = 25
        self.history = {'train': {'logits': {}, 'labels': {}},
                         'val': {'logits': {}, 'labels': None},
                         'seed': torch.seed()}
        self.global_step = -1
    
    def __str__(self) -> str:
        '''
        Returns a string version of the classifier object
        '''
        return str(torchinfo.summary(self.model.get_bert()))

    def _hyperparams_str(self) -> str:
        return f"\nRandom seed: {self.history['seed']}"

    def fit(self, 
            train_data: tuple[list[str], list[int]], 
            val_data: tuple[list[str], list[int]], 
            ) -> None:
        '''
        Fits the model to the training data and returns validation data
        
        Parameters
        -------------
        train_data : tuple[list[str], list[int]]
            training data, tuple of text data and labels
        val_data : tuple[list[str], list[int]]
            validation data, tuple of text data and labels
        '''
        print("TRAINING BASELINE RELEVANCE")
        # initialize dataset objects
        train_dataset = TextClassificationDataset(self.tokenizer, self.max_length, train_data[0], train_data[1])
        val_dataset = TextClassificationDataset(self.tokenizer, self.max_length, val_data[0], val_data[1])

        # initialize dataloaders
        self.train_dataloader = DataLoader(train_dataset, batch_size=self.batch_size, shuffle=True)
        self.val_dataloader = DataLoader(val_dataset, batch_size=self.batch_size)

        optimizer = torch.optim.AdamW(self.model.parameters(), lr=self.learning_rate)
        total_steps = len(self.train_dataloader) * self.num_epochs
        scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps=0, num_training_steps=total_steps)

        for epoch in range(self.num_epochs):
            print(f"Epoch {epoch + 1}/{self.num_epochs}")
            self._train(self.train_dataloader, optimizer, scheduler)
            train_labels = self.history['train']['labels'][self.global_step]
            train_logits = self.history['train']['logits'][self.global_step]
            print(f"TRAINING LOSS: {self.loss_function(train_logits, train_labels)}")

            # evaluate model using the validation dataset
            print("VALIDATION:")
            val_labels = np.array(self.history['val']['labels'])
            val_logits = self.history['val']['logits'][self.global_step]
            val_predictions = np.array(self._logits_to_binary(val_logits))
            print_report(val_labels, val_predictions)
            print("\n" * 3)

        print(self._hyperparams_str())

    def _logits_to_binary(self, logits: torch.Tensor) -> torch.Tensor:
        return torch.argmax(logits, dim=1)

    def _train(self, 
               data_loader: DataLoader, 
               optimizer: Optimizer, 
               scheduler: LambdaLR
               ) -> None:
        '''
        Internal train method
        
        Parameters
        --------------
        data_loader : DataLoader
            DataLoader object to batch data
        optimizer : Optimizer, 
            Optimizer object for model training
        scheduler : LambdaLR
            Scheduler object for model training
        '''
        epoch_num_steps = len(data_loader)
        for batch in tqdm(data_loader):
            self.model.train()
            self.global_step += 1
            optimizer.zero_grad()
            input_ids = batch['input_ids'].to(self.device)
            attention_mask = batch['attention_mask'].to(self.device)
            labels = batch['label'].to(self.device)

            logits = self.model(input_ids=input_ids, attention_mask=attention_mask)

            loss = self.loss_function(logits, labels)
            loss.backward()
            optimizer.step()
            scheduler.step()

            # save training labels and logits every step
            self.history['train']['labels'][self.global_step] = labels.detach().cpu()
            self.history['train']['logits'][self.global_step] = logits.detach().cpu()
            # save everything else every self.save_freq (25 steps)
            if (self.global_step % self.save_freq) == 0 or \
                    (self.global_step + 1) % epoch_num_steps == 0:
                # store logits on validation dataaset
                val_labels, val_logits = self.eval_logits(self.val_dataloader)
                if self.history['val']['labels'] is None:
                    self.history['val']['labels'] = val_labels
                self.history['val']['logits'][self.global_step] = val_logits

    def save_model(self, dir_path: str, name: str) -> None:
        '''
        Saves the model to file {name}_classifier.pth

        Parameters
        -----------------
        dir_path : str
            directory path to save files
        name : str
            name of the classifier (affects the file name)
        '''
        self.history['global_step'] = self.global_step
        with open(os.path.join(dir_path, name + "_history.pkl"), 'wb') as f:
            pickle.dump(self.history, f)
        torch.save(self.model.state_dict(), os.path.join(dir_path, name + "_classifier.pth"))

    def load_model(self, dir_path: str, name: str) -> None:
        '''
        Loads the model from classifier files

        Parameters
        -----------------
        dir_path : str
            directory path to load files
        name : str
            name of the classifier
        '''
        with open(os.path.join(dir_path, name + "_history.pkl"), 'rb') as f:
            self.history = pickle.load(f)
        self.global_step = self.history['global_step']
        self.model.load_state_dict(torch.load(os.path.join(dir_path, name + "_classifier.pth")))

    def get_history(self) -> dict:
        '''
        Returns the model training history
        '''
        return self.history

    def _rolling_average(self, lst: list[float], window_size: int=25) -> list[int]:
        '''
        Given a list of floats, returns a rolling average of the values with padding
        
        Parameters
        -------------------
        lst : list[float]
            list of float values
        window_size : int
            window size of the rolling average
        '''
        length = len(lst)
        ret = [None] * length
        lst = [lst[0]] * (window_size // 2) + lst + [lst[-1]] * (window_size // 2)
        for i in range(length):
            ret[i] = sum(lst[i : i + window_size]) / window_size
        return ret

    def plot_history(self, type_s: list[str]) -> None:
        '''
        Plots metrics over time, loss or f1

        Parameters
        --------------------
        type_s : list[str]
            A list of what metrics to plot, 'loss' or 'f1'
        step : int
            The step to plot the history graphs, default is the last step of training
        '''
        num_plots = len(type_s)
        num_cols = 2
        num_rows = (num_plots // num_cols) + (num_plots % num_cols)
        fig, axes = plt.subplots(num_rows, num_cols, figsize=(6 * num_cols, 5 * num_rows))
        axes = axes.flatten()
        
        train_logits = self.history['train']['logits']
        train_labels = self.history['train']['labels']
        val_logits = self.history['val']['logits']
        val_labels = self.history['val']['labels']

        for ax, tp in zip(axes, type_s):
            if tp == 'loss':
                metric = lambda labels, logits: self.loss_function(logits, labels)
            elif tp == 'f1':
                def _logits_to_f1(labels, logits):
                    labels = np.array(labels)
                    binary_predictions = np.array(self._logits_to_binary(logits))
                    return f1_score(labels, binary_predictions, average='micro', zero_division=0.0)
                metric = _logits_to_f1
            else: 
                continue
            
            train_times = range(self.history['global_step'] + 1)
            train_metric = [metric(train_labels[tm], train_logits[tm]) for tm in train_times]
            train_metric = self._rolling_average(train_metric)
            val_times = sorted(list(self.history['val']['logits'].keys()))
            val_metric = [metric(val_labels, val_logits[tm]) for tm in val_times]

            ax.plot(train_times, train_metric, label='train', linewidth=1, alpha=0.8)
            ax.plot(val_times, val_metric, label='validation')
            ax.set_xlabel('step (batch size 16)')
            ax.set_ylabel(tp)
            ax.legend()
            ax.set_title(f'Training/Validation {tp.capitalize()}')

        plt.tight_layout()
        plt.show()

    def eval_logits(self, data_loader: DataLoader) -> tuple[torch.Tensor, torch.Tensor]:
        '''
        Given a data_loader object with labels, returns labels and logits

        Parameters
        ---------------------
        data_loader : DataLoader
            DataLoader object to batch data
        '''
        self.model.eval()
        labels = []
        logits = []
        with torch.no_grad():
            for batch in tqdm(data_loader, leave=False, file=sys.stderr):
                input_ids = batch['input_ids'].to(self.device)
                attention_mask = batch['attention_mask'].to(self.device)
                lb = batch['label'].to(self.device)

                lg = self.model(input_ids=input_ids, attention_mask=attention_mask)

                labels.append(lb.cpu())
                logits.append(lg.cpu())
        labels = torch.cat(labels, dim=0)
        logits = torch.cat(logits, dim=0)
        return labels, logits

    def evaluate(self, data_loader: DataLoader) -> tuple[ArrayLike, ArrayLike]:
        '''
        Given a data_loader object with labels, returns labels and logits

        Parameters
        ---------------------
        data_loader : DataLoader
            DataLoader object to batch data
        '''
        labels, logits = self.eval_logits(data_loader)
        return np.array(labels), np.array(logits)

    def predict(self, test_data: list[str]) -> ArrayLike:
        '''
        Returns binary predictions given test data

        Parameters:
        ------------------
        test_data : list[str]
            input data
        '''
        test_dataset = TextClassificationDataset(self.tokenizer, self.max_length, test_data)
        test_data_loader = DataLoader(test_dataset, batch_size=self.batch_size)
        
        self.model.eval()
        logits = []
        with torch.no_grad():
            for batch in tqdm(test_data_loader):
                input_ids = batch['input_ids'].to(self.device)
                attention_mask = batch['attention_mask'].to(self.device)

                lg = self.model(input_ids=input_ids, attention_mask=attention_mask)
                logits.append(lg.cpu())

            logits = torch.cat(logits, dim=0)
        return np.array(self._logits_to_binary(logits))