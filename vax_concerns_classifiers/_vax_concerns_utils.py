# custom libraries
from ._vax_concerns_base_classifier import VaxConcernsBaseClassifier
from ._vax_concerns_dataset import TextClassificationDataset
from ._vax_concerns_bert import BERTClassifier
# data libraries
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
import json
# ML libraries
import torch
from torch import nn
from torch.utils.data import DataLoader
from transformers import BertTokenizer, get_linear_schedule_with_warmup
import onnxruntime as ort
# helper libraries
from sklearn.metrics import f1_score, recall_score, precision_score, precision_recall_curve, confusion_matrix
from sklearn import metrics
from tqdm import tqdm
import torchinfo
import pickle
import os
import sys
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
# documentation libraries
from numpy.typing import ArrayLike
from torch.optim import Optimizer
from torch.optim.lr_scheduler import LambdaLR

CATEGORIES = ['1', '1.1', '1.2', '1.3', 
              '2', '2.1', '2.2', '2.3', '2.4', '2.5', 
              '3', '3.1', '3.2', '3.3', '3.4', '3.5', 
              '4', '4.1', '4.2', 
              '5', '5.1', '5.2', '5.3', '5.4']
DESCRIPTIONS = [
    {"cat": "1", "name": "Issues with Research", "desc": """Criticism of the research of vaccines, whether attacking the quantity or quality of existing research, or generally making the point that science and studies cannot tell us things for certain. Equating inconclusive or bad science to not trusting vaccines."""},
    {"cat": "1.1", "name": "Lacking Quantity", "desc": """Argues that there is not enough research to answer a specific question or concern regarding vaccines. In this view, the implied solution is to conduct more scientific experiments."""},
    {"cat": "1.2", "name": "Poor Quality", "desc": """Attacks elements of some existing piece of vaccine research to invalidate it or cast doubts on its results. The implied solution is to redo the experiment or analysis to fix the issues of quality."""},
    {"cat": "1.3", "name": "Fallible science", "desc": """Raises doubt in knowledge regarding vaccines based on the fact that you can never be 100% sure of research conclusions. This view implies that more or better experiments will not solve the issue."""},
    {"cat": "2", "name": "Lack of Benefits", "desc": """Makes the argument that getting a vaccine is not important due to it's ineffectiveness (poor efficacy) or the lack of need for it. There are many rationales, such as claiming herd immunity or natural immunity are better alternatives to vaccines, that the disease the vaccine addresses is not dangerous or common, or that there are better medical alternatives."""},
    {"cat": "2.1", "name": "Imperfect protection", "desc": """Argues that efficacy, the rate of successful protection against disease, is low or imperfect. You may still get the disease even if you are vaccinated."""},
    {"cat": "2.2", "name": "Herd immunity", "desc": """Argues that you may already receive protection from disease on the basis that others around you are immune, whether naturally or through a vaccine."""},
    {"cat": "2.3", "name": "Natural immunity", "desc": """Argues that immunity that comes naturally from contracting the virus or disease is preferable or comparable to the immunity from vaccines."""},
    {"cat": "2.4", "name": "Insufficient risk", "desc": """Claims that the virus or disease which vaccines aim to prevent is not common or not dangerous. Equivalently, this can be states as ‘risks are exaggerated’."""},
    {"cat": "2.5", "name": "Existing alternatives", "desc": """Any claims that some medical alternative (whether accepted by the medical community or not) could replace vaccination. The potential alternatives can be varied, anything from preventions like 'hand washing' to cures like 'herbal medicine' or 'respirators'."""},
    {"cat": "3", "name": "Health Risks", "desc": """Claims that the vaccine could be harmful to your health. Discussion of harmful ingredients in vaccines, claiming you can directly transmit disease from the vaccine, talk of possible side-effects, that the schedule/rate at which vaccines are given out is dangerous, or that certain high-risk individuals may be more likely to be harmed by the vaccine."""},
    {"cat": "3.1", "name": "Direct transmission", "desc": """Concerns of contracting or spreading virus/disease through vaccination originally meant to protect against the disease"""},
    {"cat": "3.2", "name": "Harmful ingredients", "desc": """Attributes the cause of health risks to some ingredient(s) included in vaccines that are potentially hazardous."""},
    {"cat": "3.3", "name": "Specific side effects", "desc": """Cites specific undesirable outcomes which may occur from vaccination."""},
    {"cat": "3.4", "name": "Dangerous delivery", "desc": """Attributes the cause of health risks to the practices of vaccine delivery or circumstances around the vaccination rather than the vaccine contents."""},
    {"cat": "3.5", "name": "High-risk individuals", "desc": """Attributes the cause of health risks to be due to some people’s individualized reactions to vaccines. This claim states that some people of certain demographics or health conditions may be at higher risk than others."""},
    {"cat": "4", "name": "Disregard of Individual Rights", "desc": """This argument is closely related to the attacks on vaccine mandates/mass population at large. It may include arguments about religious or philosophical/ethical beliefs, individual rights to choose and rights to freedom/autonomy over your own body."""},
    {"cat": "4.1", "name": "Religious and Ethical Beliefs", "desc": """Points out conflicting views between religion/moral beliefs and vaccination, arguing that such religious/moral beliefs are incompatible with getting vaccinated."""},
    {"cat": "4.2", "name": "Right to Autonomy", "desc": """Argues that not getting vaccinated is your human right. This argument views vaccine mandates as a form of infringement on rights but may stretch to state that any consequence imposed for not getting vaccinated interferes with the right to choose."""},
    {"cat": "5", "name": "Untrustworthy Actors", "desc": """Attacks those involved in vaccines in some way. It might be claims of incompetency, suggesting profit motives which are corrupt, criticism of social media and news censorship regarding vaccine opinion, and even conspiracy theories involving anyone from private individuals to companies to governments."""},
    {"cat": "5.1", "name": "Incompetence", "desc": """Claims that some persons or organizations involved in vaccination cannot be trusted due to their incompetence. If trusted, there may be negative consequences due to errors, mistakes, stupidity, etc."""},
    {"cat": "5.2", "name": "Profit motives", "desc": """Claims that profit motives involved in vaccination create bad incentives for decision makers. Money has to be identified as the perverse motive behind decisions."""},
    {"cat": "5.3", "name": "Censorship", "desc": """Claims that anti-vax people, ideas, or data is being actively silenced or hidden by people in power."""},
    {"cat": "5.4", "name": "Conspiracy", "desc": """Claims of secret plans or activities organized by people in power, either to use vaccination for some hidden purpose or to influence/change something about vaccinations."""},
]

def print_report(labels: ArrayLike, predictions: ArrayLike) -> None:
    '''
    Prints precision, recall, and f1 scores, along with a classification report

    Parameters
    ----------------
    labels : ArrayLike
        the ground truth labels
    predictions : ArrayLike
        model predictions
    '''
    from sklearn.metrics import precision_score, recall_score, f1_score, classification_report
    
    precision = precision_score(labels, predictions, average='micro', zero_division=0.0)
    recall = recall_score(labels, predictions, average='micro', zero_division=0.0)
    f1 = f1_score(labels, predictions, average='micro', zero_division=0.0)
    report = classification_report(labels, predictions, zero_division=0.0)

    print("Precision: {}%".format(precision * 100))
    print("Recall: {}%".format(recall * 100))
    print("f1: {}%".format(f1 * 100))
    print(report)