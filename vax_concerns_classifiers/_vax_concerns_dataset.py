import torch
from torch.utils.data import Dataset
from typing import List, Optional
from transformers import BertTokenizer

class TextClassificationDataset(Dataset):
    '''
    Wrapper dataset class for text
    '''
    def __init__(self, tokenizer: BertTokenizer, max_length: int, texts: List[str], labels: Optional[List[List[int]]]=None) -> None:
        '''
        Parameters
        ------------
        tokenizer : BertTokenizer
            the text tokenizer
        max_length : int
            max string length the tokenizer can accept
        texts : List[str]
            the dataset of text, represented as a list of strings
        labels : Optional[List[List[int]]] = None
            a list of multi-hot encoding labels, the labels are optional
        '''
        self.tokenizer = tokenizer
        self.max_length = max_length
        self.texts = texts
        self.labels = labels

    def __len__(self):
        '''
        Gets the number of samples
        '''
        return len(self.texts)

    def __getitem__(self, idx: int):
        '''
        Given an index, gets the encoding for a text sample

        Parameters
        -------------
        idx : int
            the sample index
        '''
        text = self.texts[idx]
        encoding = self.tokenizer(text, return_tensors='pt', max_length=self.max_length, padding='max_length', truncation=True)
        if self.labels is None:
            return {'input_ids': encoding['input_ids'].flatten(), 
                    'attention_mask': encoding['attention_mask'].flatten()}
        
        label = self.labels[idx]
        return {'input_ids': encoding['input_ids'].flatten(), 
                'attention_mask': encoding['attention_mask'].flatten(), 
                'label': torch.tensor(label)}