(function($) {
    $(function() {
        const results = JSON.parse(sessionStorage.getItem('cls_results'));
        var descriptions;
        if (results) {
            if (results.is_highlighted) {
                generate_website_view(results.highlighted_text);
                set_multi_select(results.parent_to_children);
                descriptions = results.classes;
            } else {
                generate_text_view(results.highlight_text, results.classes, results.predictions);
            }
        }
        
        $('.form-check-input').on('change', function() {
            clear_highlighting();
            highlight_text();
        });

        function generate_text_view(highlighted_text, classes, predictions) {
            $('#text-view').empty();
            $('#text-view').text(highlighted_text);
            $('#text-classification').empty();
            var classification = "This text has concerns with:<br><ul>";
            for (let i = 0; i < predictions[0].length; i++) {
                if (predictions[0][i] === 1) {
                    classification += "<li>" + classes[i].name + "</li>";
                } 
            }
            classification += "</ul>";
            $('#text-classification').html(classification);
        }

        function generate_website_view(highlighted_text) {
            $('#website-view').html(highlighted_text);
        }

        function clear_highlighting() {
            $('[class^="highlight-"]').removeClass('highlighted');
            $('#website-view [data-toggle="tooltip"]').tooltip('dispose');
            $('#website-view [data-toggle="tooltip"]').attr('title', '').attr('data-bs-original-title', '');
        }

        function highlight_text() {
            let curr_title;
            for (let i = 0; i < descriptions.length; i++) {
                if ($(`#checkbox-${i}`).is(':checked')) {
                    $(`.highlight-${i}`).addClass('highlighted');
                    curr_title = $(`.highlight-${i}`).attr('data-bs-original-title') || '';
                    $(`.highlight-${i}`).attr('data-bs-original-title', `${curr_title} ${descriptions[i].name}`);
                    $(`.highlight-${i}`).tooltip('dispose').tooltip();
                }
            }
        }

        // set the checkbox multi-select behavior 
        function set_multi_select(parent_to_children) {
            for (let p in parent_to_children) {
                $(`#checkbox-${p}`).on('click', function() {
                    for (let c of parent_to_children[p]) {
                        $(`#checkbox-${c}`).prop('checked', $(this).is(':checked'));
                    }
                })
            }
        }
    });
})(jQuery);