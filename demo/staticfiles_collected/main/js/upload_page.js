(function($) {
    $(function() {
        $('#file-example-link').on('click', function() {
            // TODO: fill file input with example
        });

        $('#text-example-link').on('click', function() {
            $('#upload-text-input').val("COVID-19 vaccines were developed too quickly to be safe. Vaccines cause severe side effects. I'm in good health and don't get sick often, so I don't need the COVID-19 vaccine. I’ve had COVID-19, so I won’t benefit from vaccination.");
            update_counter();
        });

        $('#url-example-link').on('click', function() {
            $('#upload-url-input').val('https://www.aaaai.org/tools-for-the-public/conditions-library/allergies/vaccine-myth-fact');
        });

        const textarea = $('#upload-text-input');
        const wordCounter = $('#word-counter');
        const maxWords = 125;

        function get_num_words() {
            return textarea.val().trim().split(/\s+/).filter(word => word.length > 0).length;
        };

        function update_counter() {
            const wordCount = get_num_words();
            wordCounter.text(`${wordCount}/${maxWords} words`);
        }

        textarea.on('input', function () {
            update_counter();
        });

        const file_tab = $('#tab-file');
        const text_tab = $('#tab-text');
        const url_tab = $('#tab-url');
        
        const file_section = $('#file-section');
        const file_desc = $('#file-formats-desc')
        const text_section = $('#text-section');
        const url_section = $('#url-section');
        
        function hideAllSections() {
            file_section.addClass('d-none');
            file_desc.addClass('d-none');
            text_section.addClass('d-none');
            url_section.addClass('d-none');
            file_tab.removeClass('active');
            text_tab.removeClass('active');
            url_tab.removeClass('active');
        }
        
        function showSection(section, tab) {
            hideAllSections();
            section.removeClass('d-none');
            tab.addClass('active');
        }
        
        file_tab.on('click', function() {
            showSection(file_section, file_tab);
            file_desc.removeClass('d-none');
        });
        
        text_tab.on('click', function() {
            showSection(text_section, text_tab);
        });
        
        url_tab.on('click', function() {
            showSection(url_section, url_tab);
        });

        function start_loading() {
            $('#navbar li').addClass('disabled-tab');
            $('#tab-div label').addClass('disabled')

            $('#upload-file-input').prop('disabled', true);
            textarea.prop('disabled', true);
            $('#upload-url-input').prop('disabled', true);

            $('#file-submit').prop('disabled', true);
            $('#text-submit').prop('disabled', true);
            $('#url-submit').prop('disabled', true);

            $('#file-example-link').addClass('disabled-tab');
            $('#text-example-link').addClass('disabled-tab');
            $('#url-example-link').addClass('disabled-tab');

            show_element($('#spinner'), true);
        };

        function stop_loading() {
            $('#navbar li').removeClass('disabled-tab');
            $('#tab-div label').removeClass('disabled')

            $('#upload-file-input').prop('disabled', false);
            textarea.prop('disabled', false);
            $('#upload-url-input').prop('disabled', false);

            $('#file-submit').prop('disabled', false);
            $('#text-submit').prop('disabled', false);
            $('#url-submit').prop('disabled', false);

            $('#file-example-link').removeClass('disabled-tab');
            $('#text-example-link').removeClass('disabled-tab');
            $('#url-example-link').removeClass('disabled-tab');

            hide_all_loading();
        };

        function show_element(e, show) {
            if (show) {
                e.removeClass('d-none').addClass('d-flex');
            } else {
                e.removeClass('d-flex').addClass('d-none');
            }
        }

        function hide_all_loading() {
            show_element($('#spinner'), false);
            show_element($('#success-msg'), false);
            show_element($('#no-data-msg'), false);
            show_element($('#error-msg'), false);
        }

        $('#upload-file-form').on('submit', function(event) {
            event.preventDefault();
            if (!$('#upload-file-input').val()) {
                hide_all_loading();
                show_element($('#no-data-msg'), true);
                return;
            };

            var form_data = new FormData(this);
            form_data.append('data_type', 'file')
            start_loading();
            ajax_request(form_data);
        });

        $('#upload-text-form').on('submit', function(event) {
            event.preventDefault();
            if (!textarea.val()) {
                hide_all_loading();
                show_element($('#no-data-msg'), true);
                return;
            };

            if (get_num_words() > maxWords) {
                // TODO: put red text
                return;
            }
            var form_data = new FormData(this);
            form_data.append('data_type', 'text')
            start_loading();
            ajax_request(form_data);
        });

        $('#upload-url-form').on('submit', function(event) {
            event.preventDefault();
            if (!$('#upload-url-input').val()) {
                hide_all_loading();
                show_element($('#no-data-msg'), true);
                return;
            };

            var form_data = new FormData(this);
            form_data.append('data_type', 'url')
            start_loading();
            ajax_request(form_data);
        });

        function ajax_request(form_data) {
            $.ajax({
                url: '/data-upload/',
                type: 'POST',
                data: form_data,
                processData: false,
                contentType: false,
                success: function(response) {
                    if (response.task_id) {
                        poll_task_status(response.task_id);
                    } else {
                        stop_loading();
                        $('#error-msg').find('p').text('POST request success with no response.task_id');
                        show_element($('#error-msg'), true);
                    }
                },
                error: function(xhr, status, error){
                    stop_loading();
                    if (xhr.responseJSON && xhr.responseJSON.error) {
                        $('#error-msg').find('p').text('An error occurred: ' + xhr.responseJSON.error);
                    } else {
                        $('#error-msg').find('p').text('An error occurred');
                    }
                    show_element($('#error-msg'), true);
                }
            });
        }

        function poll_task_status(task_id) {
            const poll_interval = 3000; // 3 seconds
            check_status();

            function check_status() {
                $.ajax({
                    url: `/task-status/${task_id}/`,
                    type: 'GET',
                    success: function(response) {
                        if (response.status === 'completed') {
                            stop_loading();
                            show_element($('#success-msg'), true);
                            sessionStorage.setItem('cls_results', JSON.stringify(response.cls_results));
                        } else if (response.status === 'in_progress') {
                            setTimeout(check_status, poll_interval); // Poll again after a delay
                        } else {
                            stop_loading();
                            $('#error-msg').find('p').text('GET Request success with no response.status');
                            show_element($('#error-msg'), true);
                        }
                    },
                    error: function(xhr, status, error) {
                        stop_loading();
                        if (xhr.responseJSON && xhr.responseJSON.error) {
                            $('#error-msg').find('p').text('An error occurred: ' + xhr.responseJSON.error);
                        } else {
                            $('#error-msg').find('p').text('An error occurred');
                        }
                        show_element($('#error-msg'), true);
                    }
                });
            }
        }
    });
})(jQuery);