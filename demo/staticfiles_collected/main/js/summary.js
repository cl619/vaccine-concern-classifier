(function($) {
    $(function() {
        const results = JSON.parse(sessionStorage.getItem('cls_results'));
        if (results) {
            generate_summary_table(results, $('#summary-container'));
        }

        function generate_summary_table(response, table_container) {
            table_container.empty();

            var classes = response.classes;
            var colors = response.colors;
            var stats = response.summary_stats;
            var word_cloud_paths = response.word_cloud_paths;
            var texts = response.texts;
            var predictions = response.predictions;
            var parent_to_children = response.parent_to_children;

            // make table with headers
            var table = $('<table>', {class:'table text-center'});
            var head_row = $('<tr>')
                .append($('<th>', {scope: 'col', class: 'col-3'}).text('Concern'))
                .append($('<th>', {scope: 'col', class: 'col-3'}).text('Word cloud of relevant spans'))
                .append($('<th>', {scope: 'col', class: 'col-6'}).text('Spans found raising concern'))
            table.append($('<thead>').append(head_row));

            var body = $('<tbody>');
            for (let i = 0; i < classes.length; i++) {
                var cat = classes[i].cat;
                var color = colors[i];

                var row;
                var row_header;
                // number column
                if (!cat.includes('.')) {
                    row = $('<tr>');
                    row_header = $('<th>', {'class':'text-start', 'scope':'row', 'style':`background-color:${color}`});
                    row_header.append($('<div>', {'class':'form-check'})
                        .append($('<input>', {'class':'form-check-input','type':'checkbox','id':`checkbox-${i}`}))
                        .append($('<span>', {'class':'clickable', 'data-bs-toggle':'collapse','data-bs-target':`#row_collapse_${cat}`,'aria-expanded':'false','aria-controls':`row_collapse_${cat}`})
                                .append($('<div>', {'class':'row m-0'})
                                    .append($('<div>', {'class':'col-10'}).append($('<span>').text(`${cat}. ${classes[i].name}`)))
                                    .append($('<div>', {'class':'col-2'}).append($('<i>', {'class':'bi bi-chevron-down m-2'})))
                                )
                        )
                    );
                } else {
                    row = $('<tr>', {'class':'collapse collapse-transition', 'id':`row_collapse_${cat.split('.')[0]}`});
                    row_header = $('<th>', {'class':'text-start', 'scope':'row', 'style':`background-color:${color}`});
                    row_header.append($('<div>', {'class':'form-check'})
                        .append($('<div>', {'class':'container'})
                            .append($('<div>', {'class':'row'})
                                .append($('<div>',{'class':'col-2'}).append($('<input>', {'class':'form-check-input ms-3','type':'checkbox','id':`checkbox-${i}`})))
                                .append($('<div>',{'class':'col-10'}).append($('<span>').text(`${cat}. ${classes[i].name}`)))
                            )
                        )
                    );
                }
                row.append(row_header);

                // word cloud column
                row.append($('<td>', {'style':`background-color:${color}`}).append($('<img>', {'class': 'img-fluid', 'src': word_cloud_paths[i]})));

                // carousel column
                var examples_carousel = $('<div>', {'id':`carousel_${i}`, 'class':'carousel carousel-dark slide'});
                var carousel_inner = $('<div>', {'class': 'carousel-inner'});
                var active = "active";
                for (let sample_idx = 0; sample_idx < predictions.length; sample_idx++) {
                    if (predictions[sample_idx][i] == 1) {
                        // console.log(texts[sample_idx].length, font_size);
                        carousel_inner.append(
                            $('<div>', {'class': `carousel-item ${active}`, 'style':'height:100%'})
                                .append($('<div>', {'class': 'carousel-caption', 'style':`overflow-y:scroll; height:100%;'`}) // font-size:calc(${font_size}vw); 
                                    .append($('<p>').text(texts[sample_idx]))));
                        if (active === 'active') {
                            active = '';
                        }
                    }
                }
                if (!carousel_inner.is(':empty')) {
                    examples_carousel.append(carousel_inner);
                    examples_carousel
                    .append($('<button>', {'class':"carousel-control-prev", 'type':'button', 'data-bs-target':`#carousel_${i}`, 'data-bs-slide':'prev'})
                        .append($('<span>', {'class':'carousel-control-prev-icon', 'aria-hidden':'true'}))
                        .append($('<span>', {'class':'visually-hidden'}).text('Prev')));
                    examples_carousel
                    .append($('<button>', {'class':"carousel-control-next", 'type':'button', 'data-bs-target':`#carousel_${i}`, 'data-bs-slide':'next'})
                        .append($('<span>', {'class':'carousel-control-next-icon', 'aria-hidden':'true'}))
                        .append($('<span>', {'class':'visually-hidden'}).text('Next')));
                    row.append($('<td>', {'style':`background-color:${color}`}).append(examples_carousel).append($('<small>', {'id':`counter-${i}`}).text(`1/${stats.frequencies[i]}`)));
                } else {
                    row.append($('<td>', {'style':`background-color:${color}`}).append(examples_carousel));
                }
                
                body.append(row);
            }
            table.append(body);
            
            table_container.append(table);
            table_container.append($('<a>', {'id':'download-examples','class':'m-2 btn btn-primary'}).append($('<i>', {'class':'bi bi-download'})));
            table_container.append($('<span>').text("Download selected concerns"));

            // set the download examples button
            $('#download-examples').on('click', function(e) {
                e.preventDefault();
                const checkboxes = [];
                let is_checked;
                let download = false;
                for (let i = 0; i < classes.length; i++) {
                    is_checked = $(`#checkbox-${i}`).is(':checked');
                    checkboxes.push(is_checked);
                    download = download || is_checked;
                }
                if (download) { 
                    window.location.href = `/summary/download/?checkboxes=${JSON.stringify(checkboxes)}`;
                } else {

                }
            });

            // set the checkbox multi-select behavior 
            for (let p in parent_to_children) {
                $(`#checkbox-${p}`).on('click', function() {
                    for (let c of parent_to_children[p]) {
                        $(`#checkbox-${c}`).prop('checked', $(this).is(':checked'));
                    }
                })
            }
            
            // set the carousel page counters
            for (let i = 0; i < classes.length; i++) {
                if ($(`#carousel_${i}`).length) {
                    $(`#carousel_${i}`).on('slid.bs.carousel', function () {
                        var curr_idx = $(`#carousel_${i} .carousel-item.active`).index() + 1;
                        $(`#counter-${i}`).text(`${curr_idx}/${stats.frequencies[i]}`);
                    });
                }
            }

            // set the animation for clicking dropdowns
            $('.clickable').on('click', function() {
                $(this).find('i.bi').toggleClass('bi-chevron-down bi-chevron-up');
            });
        }
    });
})(jQuery);