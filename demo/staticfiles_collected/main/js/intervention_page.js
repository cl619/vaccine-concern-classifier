(function($) {
    $(function() {
        const url_params = new URLSearchParams(window.location.search);
        $('#concern').text(url_params.get('concern'));
        fetch_opinion();
        fetch_intervention();
        window.fetch_opinion = fetch_opinion;
        window.fetch_intervention = fetch_intervention;

        function fetch_opinion() {
            $.ajax({
                url: 'fetch-opinion/',
                method: 'GET',
                success: function(response) {
                    $('#opinion-text-area').val(response.opinion);
                },
                error: function(xhr, status, error) {
                    console.error('Error fetching data:', error);
                }
            });
        };

        function fetch_intervention() {
            $.ajax({
                url: 'fetch-intervention/',
                method: 'GET',
                success: function(response) {
                    $('#intervention-text-area').val(response.intervention);
                },
                error: function(xhr, status, error) {
                    console.error('Error fetching data:', error);
                }
            });
        };
    });
})(jQuery);