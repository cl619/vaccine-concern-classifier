# forms.py
from django import forms

class MultilabelForm(forms.Form):
    uploaded_file = forms.FileField(required=False)
    uploaded_text = forms.CharField(required=False)
    uploaded_url = forms.CharField(required=False)