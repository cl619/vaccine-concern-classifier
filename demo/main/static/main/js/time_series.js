(function($) {
    $(function() {
        $.ajax({
            url: '/fetch-time/',
            data: {
                selected: selection,
            },
            method: 'GET',
            success: function(response) {
                render_elements(selection, response.classes);
                generate_iframe(response.highlighted_text);
                generate_example_table(selection, response.predictions, response.texts);
            },
            error: function(xhr, status, error) {
                console.error('Error fetching data:', error);
            }
        });


        const csv = `Time,Temperature
            2020-02-15,-8.25
            2020-02-16,-8.08
            2020-02-17,-8.41
            2020-02-18,-8.2`;
        const csv2 = `Time,Temperature
            2020-02-15,1
            2020-02-16,-8.08
            2020-02-18,-8.41
            2020-02-19,-8.2`;

        const csvToChartData = csv => {
            const lines = csv.trim().split('\n');
            lines.shift();
            return lines.map(line => {
                const [date, temperature] = line.split(',');
                return {
                x: date,
                y: temperature
                }
            });
        };

        const ctx = document.querySelector("#line-chart").getContext('2d');
        const config = {
            type: 'line',
            data: {
                labels: [],
                datasets: [
                    {
                        data: csvToChartData(csv), // Temperature data
                        label: "Temperature",
                        borderColor: "#3e95cd",
                        fill: false,
                        lineTension: 0,
                    },
                    {
                        data: csvToChartData(csv2), // Humidity data
                        label: "Humidity",
                        borderColor: "#8e5ea2", // Choose a different color for the humidity line
                        fill: false,
                        lineTension: 0,
                    }
                ]
            },
            options: {
                scales: {
                    xAxes: [{
                        type: 'time',
                        time: {
                            unit: 'day',
                        },
                        distribution: 'linear',
                    }],
                },
                title: {
                    display: false,
                }
            }
        };
        new Chart(ctx, config);
    });
})(jQuery);