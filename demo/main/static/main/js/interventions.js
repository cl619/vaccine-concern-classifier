(function($) {
    $(function() {
        const concern_text_area = $('#concern-text-area');
        const word_counter = $('#word-counter');
        const max_words = 125;

        function get_num_words() {
            return concern_text_area.val().trim().split(/\s+/).filter(word => word.length > 0).length;
        };

        function update_counter() {
            const wc = get_num_words();
            word_counter.text(`${wc}/${max_words} words`);
        }

        concern_text_area.on('input', function () {
            update_counter();
        });

        function start_loading() {
            $('#navbar li').addClass('disabled-tab');
            $('#tab-div label').addClass('disabled')
            $('#text-submit').prop('disabled', true);
            show_element($('#spinner'), true);
        };

        function stop_loading() {
            $('#navbar li').removeClass('disabled-tab');
            $('#tab-div label').removeClass('disabled')
            $('#text-submit').prop('disabled', false);
            hide_all_loading();
        };

        function show_element(e, show) {
            if (show) {
                e.removeClass('d-none').addClass('d-flex');
            } else {
                e.removeClass('d-flex').addClass('d-none');
            }
        }

        function hide_all_loading() {
            show_element($('#spinner'), false);
            show_element($('#success-msg'), false);
            show_element($('#no-data-msg'), false);
            show_element($('#error-msg'), false);
        }

        $('#upload-concern-form').on('submit', function(event) {
            event.preventDefault();
            if (!concern_text_area.val()) {
                hide_all_loading();
                show_element($('#no-data-msg'), true);
                return;
            };
            if (get_num_words() > max_words) {
                // TODO: put red text
                return;
            }
            var form_data = new FormData();
            form_data.append('concern-text', concern_text_area.val())
            start_loading();
            ajax_request(form_data);
        });

        function ajax_request(form_data) {
            $.ajax({
                url: '/concern-upload/',
                type: 'POST',
                data: form_data,
                processData: false,
                contentType: false,
                success: function(response) {
                    if (response.task_id) {
                        poll_task_status(response.task_id);
                    } else {
                        stop_loading();
                        $('#error-msg').find('p').text('POST request success with no response.task_id');
                        show_element($('#error-msg'), true);
                    }
                },
                error: function(xhr, status, error){
                    stop_loading();
                    if (xhr.responseJSON && xhr.responseJSON.error) {
                        $('#error-msg').find('p').text('An error occurred: ' + xhr.responseJSON.error);
                    } else {
                        $('#error-msg').find('p').text('An error occurred');
                    }
                    show_element($('#error-msg'), true);
                }
            });
        }

        function poll_task_status(task_id) {
            const poll_interval = 3000; // 3 seconds
            check_status();

            function check_status() {
                $.ajax({
                    url: `/task-status/${task_id}/`,
                    type: 'GET',
                    success: function(response) {
                        if (response.status === 'completed') {
                            stop_loading();
                            show_element($('#success-msg'), true);
                            update_interventions(response.interventions);
                        } else if (response.status === 'in_progress') {
                            setTimeout(check_status, poll_interval); // Poll again after a delay
                        } else {
                            stop_loading();
                            $('#error-msg').find('p').text('GET Request success with no response.status');
                            show_element($('#error-msg'), true);
                        }
                    },
                    error: function(xhr, status, error) {
                        stop_loading();
                        if (xhr.responseJSON && xhr.responseJSON.error) {
                            $('#error-msg').find('p').text('An error occurred: ' + xhr.responseJSON.error);
                        } else {
                            $('#error-msg').find('p').text('An error occurred');
                        }
                        show_element($('#error-msg'), true);
                    }
                });
            }
        }

        function update_interventions(interventions) {
            const intervention_list = $('#intervention-list');
            intervention_list.empty();
            interventions.forEach(intervention => {
                intervention_list.append($('<li>').text(intervention));
            });
        }
    });
})(jQuery);