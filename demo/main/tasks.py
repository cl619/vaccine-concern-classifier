import os
from vax_concerns_classifiers import MultilabelWeighted
from celery import shared_task

weighted = MultilabelWeighted()
weighted.load_model(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'classifiers'), 'weighted_log1p')

@shared_task
def run_prediction(texts):
    return weighted.predict(texts).tolist()