from vax_concerns_classifiers import CATEGORIES
import os
import pandas as pd
import numpy as np
import json
import pymupdf
from docx import Document
from nltk.tokenize import sent_tokenize, word_tokenize

from PIL import Image
from django.conf import settings
from wordcloud import WordCloud

TOKENIZER_MAX_LENGTH = 128
COLORS = ["#fff2cc"] + ["#fff7e0"] * 3 + \
    ["#d9ead3"] + ["#dce3da"] * 5 + \
    ["#c9daf8"] + ["#d1dcf0"] * 5 + \
    ["#fce5cd"] + ["#fceddc"] * 2 + \
    ["#ead1dc"] + ["#e6d8de"] * 4

def hex_to_rgb(hex_code):
    hex_code = hex_code.lstrip('#')
    return tuple(int(hex_code[i:i+2], 16) for i in (0, 2, 4))

def combine_tokens(tokens: list[str]):
    return ' '.join(tokens).replace(' ,', ',').replace(' .', '.').replace(' !', '!').replace(' ?', '?').replace(" '", "'").replace(" ’ ", "’").replace("“ ", "“").replace(" ”", "”").replace(" :", ":").replace("( ", "(").replace(" )", ")").replace("can not", "cannot").replace(" %","%").replace("do n't", "don't")

def chunkify(sentences: list[str]):
    ret = []
    currChunk = []
    for s in sentences:
        if len(currChunk) + len(s) <= TOKENIZER_MAX_LENGTH:
            currChunk.extend(s)
        elif len(s) <= TOKENIZER_MAX_LENGTH:
            ret.append(combine_tokens(currChunk))
            currChunk = s
        else:
            ret.append(combine_tokens(currChunk))
            ret.append(combine_tokens(s))
            currChunk = []
    if currChunk:
        ret.append(combine_tokens(currChunk))
    return ret

def parse_text(text: str) -> list[str]:
    ret = []
    for sample in text.split('\n'):
        tokenized_sentences = [word_tokenize(sent) for sent in sent_tokenize(sample)]
        ret.extend(chunkify(tokenized_sentences))
    return ret

def generate_word_clouds(texts, predictions):
    positive_texts = [[sample_index for sample_index in range(len(texts)) if predictions[sample_index][cat_index]] for cat_index in range(len(CATEGORIES))]
    positive_texts = [" ".join([texts[i] for i in pt]) if pt else "" for pt in positive_texts]

    image_paths = []
    mask = np.array(Image.open("main/static/main/images/cloud_mask.png").convert('L'))
    for cat, pt, color in zip(CATEGORIES,  positive_texts, COLORS):
        image_name = f'image_{cat}.png'
        image_path = os.path.join(settings.MEDIA_ROOT, 'images', image_name)
        os.makedirs(os.path.dirname(image_path), exist_ok=True)

        if pt:
            wordcloud = WordCloud(width=800, height=400, background_color=color, mask=mask).generate(pt)
            image = wordcloud.to_image()
        else:
            image = Image.new('RGBA', (800, 400), hex_to_rgb(color) + (255,))

        image.save(image_path)
        relative_path = os.path.join(settings.MEDIA_URL, 'images', image_name)
        image_paths.append(relative_path)
    return image_paths

def find_indices(text, find, start):
    text_substr = text[start:]
    if find in text_substr:
        find_start = text_substr.index(find) + start
        find_end = find_start + len(find)
        return [(find_start, find_end, find)]
    
    find_words = find.split(" ")
    for i in range(1, len(find_words) - 1):
        left = " ".join(find_words[:i])
        right = " ".join(find_words[i:])
        if left in text_substr and right in text_substr:
            left_start = text_substr.index(left) + start
            left_end = left_start + len(left)
            right_start = text_substr.index(right) + start
            right_end = right_start + len(right)
            return [(left_start, left_end, left), 
                    (right_start, right_end, right),]
    # print(find_words, text_substr[:600])
    # left = " ".join(find_words[:1]) 
    # right = " ".join(find_words[1:])
    # print(right[:200])
    # print(left in text_substr, right[:200] in text_substr)
    return None

def generate_highlighted_text(predictions, texts, soup_txt):
    texts_to_highlight = list()
    for sample_idx, sample_txt in enumerate(texts):
        sample_pred = np.array(predictions[sample_idx]).astype(int)
        if np.sum(sample_pred) == 0:
            continue
        texts_to_highlight.append((sample_txt, np.where(sample_pred == 1)[0]))
        
    curr_idx = 0
    for t_to_highlight, classes in texts_to_highlight:
        found = find_indices(soup_txt, t_to_highlight, curr_idx)
        classes_txt = " ".join([f"highlight-{c}" for c in classes])
        add_len = len(f"""<span class="" data-toggle="tooltip" data-placement="top" title="hi"></span>""") + len(classes_txt)
        for i, (start, end, txt) in enumerate(found):
            soup_txt = f"""{soup_txt[:start + i * add_len]}<span class="{classes_txt}" data-toggle="tooltip" data-placement="top" title="hi">{txt}</span>{soup_txt[end + i * add_len:]}"""
        curr_idx = found[-1][1] + add_len * len(found)

    return soup_txt

def generate_time_series(times, predictions):
    ret = dict()
    for class_idx in range(len(predictions[0])):
        ret[class_idx] = [sample[class_idx] for sample in predictions]
    return ret

def handle_file(uploaded_file)->list[int]:
    file_type = uploaded_file.content_type
    times = None

    # Handle CSV files
    if file_type == 'text/csv':
        df = pd.read_csv(uploaded_file)
        texts = []
        for t in df[df.columns[0]].to_list():
            texts.extend(parse_text(t))
        if len(df.columns) > 1:
            times = df[df.columns[1]].to_list()

    # Handle JSON files
    elif file_type == 'application/json':
        data = json.load(uploaded_file)
        # assumes the JSON file contains a list of texts
        texts = []
        if isinstance(data, list):
            for d in data:
                texts.extend(parse_text(d))
    
    # Handle TXT files
    elif file_type == 'text/plain':
        texts = parse_text(uploaded_file.read().decode('utf-8'))
    
    # Handle PDF files
    elif file_type == 'application/pdf':
        doc = pymupdf.Document(stream=uploaded_file.read())
        texts = []
        for page in doc:
            texts.extend(parse_text(page.get_text()))
    
    # Handle Word (DOCX) files
    elif file_type in ('application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/msword'):
        doc = Document(uploaded_file)
        texts = []
        for para in doc.paragraphs:
            texts.extend(parse_text(para.text))
    
    # Error handling
    else:
        print(f"ERROR: no handling for this file of file type {file_type}")
        texts = []

    return texts, times

def get_similarity(A, B):
    intersection = np.sum((A == 1) & (B == 1))
    union = np.sum((A == 1) | (B == 1))
    return intersection / union