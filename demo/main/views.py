from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
import tempfile
from .forms import MultilabelForm

import os, pickle
import numpy as np
from vax_concerns_classifiers import DESCRIPTIONS
from .utils import generate_word_clouds, generate_highlighted_text, handle_file, parse_text, COLORS, get_similarity
import requests
from bs4 import BeautifulSoup
import ast
from celery.result import AsyncResult
from .tasks import run_prediction

CHECKBOX_NAMES = [
    {"parent_name":"1. Issues with Research", "checkbox_id":"checkbox-0", "children":[
        {"name": "1.1. Lacking Quantity", "checkbox_id":"checkbox-1"},
        {"name": "1.2. Poor Quality", "checkbox_id":"checkbox-2"},
        {"name": "1.3. Fallible science", "checkbox_id":"checkbox-3"},
        ]},
    {"parent_name":"2. Lack of Benefits", "checkbox_id":"checkbox-4", "children":[
        {"name": "2.1. Imperfect protection", "checkbox_id":"checkbox-5"},
        {"name": "2.2. Herd immunity", "checkbox_id":"checkbox-6"},
        {"name": "2.3. Natural immunity", "checkbox_id":"checkbox-7"},
        {"name": "2.4. Insufficient risk", "checkbox_id":"checkbox-8"},
        {"name": "2.5. Existing alternatives", "checkbox_id":"checkbox-9"},
        ]},
    {"parent_name":"3. Health Risks", "checkbox_id":"checkbox-10", "children":[
        {"name": "3.1. Direct transmission", "checkbox_id":"checkbox-11"},
        {"name": "3.2. Harmful ingredients", "checkbox_id":"checkbox-12"},
        {"name": "3.3. Specific side effects", "checkbox_id":"checkbox-13"},
        {"name": "3.4. Dangerous delivery", "checkbox_id":"checkbox-14"},
        {"name": "3.5. High-risk individuals", "checkbox_id":"checkbox-15"},
        ]},
    {"parent_name":"4. Disregard of Individual Rights", "checkbox_id":"checkbox-16", "children":[
        {"name": "4.1. Religious and Ethical Beliefs", "checkbox_id":"checkbox-17"},
        {"name": "4.2. Right to Autonomy", "checkbox_id":"checkbox-18"},
        ]},
    {"parent_name":"5. Untrustworthy Actors", "checkbox_id":"checkbox-19", "children":[
        {"name": "5.1. Incompetence", "checkbox_id":"checkbox-20"},
        {"name": "5.2. Profit motives", "checkbox_id":"checkbox-21"},
        {"name": "5.3. Censorship", "checkbox_id":"checkbox-22"},
        {"name": "5.4. Conspiracy", "checkbox_id":"checkbox-23"},
        ]},
]
PARENT_TO_CHILDREN = {
    0: [1, 2, 3],
    4: [5, 6, 7, 8, 9],
    10: [11, 12, 13, 14, 15],
    16: [17, 18],
    19: [20, 21, 22, 23],
}
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
    'Accept-Language': 'en-US,en;q=0.9',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Referer': 'https://www.google.com/',
    'Connection': 'keep-alive'
}
with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'interventions_distributions.pkl'), 'rb') as f:
    distributions = pickle.load(f)

def about(request):
    return render(request, "about.html", {})

@csrf_exempt
def multilabel_data_upload(request):
    if request.method != 'POST':
        return JsonResponse({'error': 'Invalid request method.'}, status=400)

    request.session['task_type'] = 'default'
    form = MultilabelForm(request.POST, request.FILES)
    if not form.is_valid():
        return JsonResponse({'error': 'Form not valid.'}, status=400)

    uploaded_file = form.cleaned_data['uploaded_file']
    uploaded_text = form.cleaned_data['uploaded_text']
    uploaded_url = form.cleaned_data['uploaded_url']
    data_type = request.POST.get('data_type')

    soup = None
    times = None
    if data_type == 'file' and uploaded_file:
        texts, times = handle_file(request.FILES['uploaded_file'])

        soup = BeautifulSoup("""<ul class="list-group"></ul>""", 'lxml')
        ul = soup.find('ul', class_='list-group')
        for t in texts:
            new_li = soup.new_tag('li', attrs={'class': 'list-group-item'})
            new_li.string = t
            ul.append(new_li)
    elif data_type == 'text' and uploaded_text:
        texts = parse_text(uploaded_text)
    elif data_type == 'url' and uploaded_url:
        response = requests.get(uploaded_url, headers=headers)
        if response.status_code != 200:
            return JsonResponse({'error': 'Could not parse url ' + uploaded_url}, status=400)
        soup = BeautifulSoup(response.content, 'html.parser')
        texts = parse_text(soup.get_text())
    else:
        return JsonResponse({'error': 'URL request failed'}, status=400)
    
    prediction_task = run_prediction.apply_async(args=[texts])
    request.session['cls_results'] = {
        'classes': DESCRIPTIONS,
        'colors': COLORS,
        'parent_to_children': PARENT_TO_CHILDREN,
        'texts': texts,
        'is_highlighted': soup is not None,
        'pretty_soup': soup.prettify() if soup else None
    }
    return JsonResponse({'message': 'Prediction started', 'task_id': prediction_task.id})

@csrf_exempt
def task_status(request, task_id):
    task = AsyncResult(task_id)
    if task.ready():
        predictions = task.result
        task_type = request.session.get('task_type', 'default')
        if task_type == 'intervention':
            pred = np.array(predictions[0])
            matches = sorted(distributions, key=lambda x: get_similarity(pred, x[1]), reverse=True)[:5]
            matches = [m[0] for m in matches]
            return JsonResponse({'status': 'completed', 'prediction': list(pred), 'interventions': matches})
        elif task_type == 'default':
            cls_results = request.session.get('cls_results')
            texts = cls_results['texts']
            pretty_soup = cls_results['pretty_soup']

            summary_stats = {'num_samples': [len(predictions)] * len(predictions[0]),
                            'frequencies': np.sum(predictions, axis=0).tolist(),}
            word_cloud_paths = generate_word_clouds(texts, predictions)

            request.session['cls_results']['predictions'] = predictions
            request.session['cls_results']['summary_stats'] = summary_stats
            request.session['cls_results']['word_cloud_paths'] = word_cloud_paths
            request.session['cls_results']['highlighted_text'] = generate_highlighted_text(predictions, texts, pretty_soup) if pretty_soup else texts[0]

            return JsonResponse({'status': 'completed', 'cls_results': request.session['cls_results']})
    else:
        return JsonResponse({'status': 'in_progress'})

def upload_page(request):
    return render(request, 'upload_page.html', {})

def summary(request):
    return render(request, 'summary.html', {})

def generate_examples_file(request):
    checkboxes = request.GET.get('checkboxes')
    mask = np.array(ast.literal_eval(checkboxes.replace('f','F').replace('t','T')))

    predictions = request.session.get('cls_results')['predictions']
    texts = request.session.get('cls_results')['texts']
    examples = "\n".join([texts[sample_idx] for sample_idx, pred in enumerate(predictions) if np.sum(np.array(pred).astype(int) & mask)])

    with tempfile.NamedTemporaryFile(delete=False) as temp_file:
        temp_file.write(examples.encode('utf-8'))
        temp_file_name = temp_file.name

    with open(temp_file_name, 'rb') as f:
        response = HttpResponse(f.read(), content_type='text/plain')
        response['Content-Disposition'] = f"""attachment; filename=\"class_{", ".join([DESCRIPTIONS[i]['cat'] for i, inc in enumerate(mask) if inc])}.txt\""""
        response['Content-Length'] = f.tell()

    os.unlink(temp_file_name)
    return response

def explore_examples(request):
    if request.session.get('cls_results') is None:
        return render(request, 'explore_examples.html', {"is_highlighted": True, "checkbox_names": CHECKBOX_NAMES})
    return render(request, 'explore_examples.html', {"is_highlighted": request.session.get('cls_results')['is_highlighted'],
                                                     "checkbox_names": CHECKBOX_NAMES})

def time_series(request):
    return render(request, 'time_series.html', {})

def interventions(request):
    return render(request, 'interventions.html', {})

@csrf_exempt
def concern_upload(request):
    if request.method != 'POST':
        return JsonResponse({'error': 'Invalid request method.'}, status=400)

    request.session['task_type'] = 'intervention'
    uploaded_text = request.POST.get('concern-text', '')
    texts = parse_text(uploaded_text)
    prediction_task = run_prediction.apply_async(args=[texts])
    return JsonResponse({'message': 'Prediction started', 'task_id': prediction_task.id})