from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path("", views.about),
    path("about/", views.about, name="about"),

    path("data-upload/", views.multilabel_data_upload),
    path("task-status/<str:task_id>/", views.task_status, name="task-status"),
    path("upload-page/", views.upload_page, name="upload-page"),

    path("summary/", views.summary, name="summary"),
    path('summary/download/', views.generate_examples_file),

    path("explore-examples/", views.explore_examples, name="explore-examples"),

    path("interventions/", views.interventions, name="interventions"),
    path("concern-upload/", views.concern_upload),

    # path("time-series/", views.time_series, name="time-series"),

    # path("intervention-page/", views.intervention_page, name="intervention-page"),
    # path("intervention-page/fetch-opinion/", views.fetch_opinion, name="fetch-opinion"),
    # path("intervention-page/fetch-intervention/", views.fetch_intervention, name="fetch-intervention"),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) # change this later for deployment
# https://docs.djangoproject.com/en/5.0/howto/static-files/deployment/