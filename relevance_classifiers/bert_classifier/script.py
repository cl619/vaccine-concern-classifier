from vax_concerns_classifiers import RelevanceBaseline
from sklearn.model_selection import train_test_split
import pandas as pd
import os

LOCAL_DIR = os.path.dirname(os.path.abspath(__file__))
TRAIN_PATH = os.path.join(LOCAL_DIR, '..', '..', 'data', 'train', 'gpt_ann_short.csv')

# load data
train_df = pd.read_csv(TRAIN_PATH)
train_df = train_df[train_df["gpt_cleaned"].isin([0, 1])] # some responses are nan
train_texts = train_df["paragraph_content"].to_list()
train_labels = [int(x) for x in train_df["gpt_cleaned"].to_list()]

# split into training, cutoff tuning, and validation datasets
train_texts, val_texts, train_labels, val_labels = train_test_split(train_texts, train_labels, test_size=0.2, random_state=42)

relevance_baseline = RelevanceBaseline()
relevance_baseline.fit((train_texts, train_labels), 
                       (val_texts, val_labels))
relevance_baseline.save_model(LOCAL_DIR, "baseline")