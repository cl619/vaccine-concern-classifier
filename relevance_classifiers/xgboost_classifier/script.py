import os
import pandas as pd
# from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
# from sklearn.metrics import accuracy_score
from sklearn.model_selection import GridSearchCV
import matplotlib.pyplot as plt

import xgboost as xgb
from sklearn.feature_extraction.text import TfidfVectorizer

TRAIN_PATH = "../../data/train/ann_gpt_short.csv"
TEST_PATH = "../../data/test/ann_combo.csv"

train_df = pd.read_csv(TRAIN_PATH)
train_df = train_df[train_df["gpt_cleaned"].isin([0, 1])] # some responses are nan
test_df = pd.read_csv(TEST_PATH)

texts_train, texts_test, labels_train, labels_test = train_df["paragraph_content"], test_df["paragraph_content"], train_df["gpt_cleaned"], test_df["gpt_annotations"]

# Convert text data into numerical features using TF-IDF
vectorizer = TfidfVectorizer()
X_train = vectorizer.fit_transform(texts_train)
# X_test = vectorizer.transform(texts_test)

# Train the XGBoost model
model = xgb.XGBClassifier(n_jobs=-1)

param_grid = {
    'learning_rate':[0.01, 0.1, 0.2],
    'n_estimators':[50, 100, 200],
    'max_depth':[3, 5, 7],
    'min_child_weight':[1, 3, 5],
    'gamma':[0.1, 0.2, 0.3],
    'subsample':[0.6, 0.7, 0.8, 0.9],
    'reg_alpha':[1e-5, 1e-2, 0.1, 1, 100]
}
metrics = ('accuracy', 'precision', 'recall', 'f1', 'roc_auc', 'neg_log_loss')

gsearch = GridSearchCV(
    estimator = xgb.XGBClassifier(),
    param_grid = param_grid,
    scoring=metrics,
    n_jobs=-1,
    refit=False,
    cv=5,
    verbose=3
    )
gsearch.fit(X_train, labels_train)

results = pd.DataFrame(gsearch.cv_results_)
results.to_csv("xgboostResult.csv", index=False)