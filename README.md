# Vaccine Concern Classifier

## About

This research project, conducted by Christopher Li and CS PhD candidate Rickard Stureborg aims to analyze historical trends in vaccine-related text using lightweight BERT language models. Our goal is to compile and publish a large dataset of labeled vaccine-related text for use in academic and industry research.

The repository contains a vaccine relevance classifier and a multi-label classifier using knowledge distillation techniques from ChatGPT. These classifiers achieved F1 scores of 96.0% and 53.0% respectively, comparable to GPT-4’s performance. Additionally, a demo website is in the works, which will showcase these classifiers with real-time predictions and visualizations of their outputs.

## Relevance Classifier

```sh
              precision    recall  f1-score   support

           0       0.93      0.92      0.92       181
           1       0.95      0.96      0.96       323

    accuracy                           0.94       504
   macro avg       0.94      0.94      0.94       504
weighted avg       0.94      0.94      0.94       504
```
![relevance history plot](relevance_history.png "Relevance History Plot")
![relevance confusion matrix](relevance_confusion_matrix.png "Relevance Confusion Matrix")

## Multilabel Classifier

```sh
              precision    recall  f1-score   support

           0       0.69      0.54      0.61       126
           1       0.75      0.17      0.27        18
           2       0.56      0.38      0.45        72
           3       0.38      0.25      0.30        20
           4       0.82      0.40      0.54       138
           5       0.96      0.40      0.56        55
           6       1.00      0.75      0.86         4
           7       0.14      0.14      0.14         7
           8       0.78      0.13      0.22        55
           9       0.00      0.00      0.00        14
          10       0.83      0.84      0.83       377
          11       0.50      0.05      0.08        22
          12       0.84      0.58      0.68        92
          13       0.72      0.56      0.63       125
          14       0.50      0.04      0.08        24
          15       0.28      0.17      0.21        46
          16       0.90      0.68      0.78       107
          17       0.69      0.64      0.67        14
          18       0.86      0.71      0.78        96
          19       0.62      0.81      0.70       149
          20       0.00      0.00      0.00         2
          21       0.67      0.65      0.66        55
          22       0.80      0.13      0.22        31
          23       0.70      0.23      0.35        30

   micro avg       0.74      0.57      0.65      1679
   macro avg       0.62      0.39      0.44      1679
weighted avg       0.74      0.57      0.62      1679
 samples avg       0.51      0.42      0.44      1679
```
![log1p history plot](log1p_history.png "log1p History Plot")
![log1p threshold analysis plot](log1p_threshold.png "log1p Threshold Analysis Plot")

## Getting started

1. Clone the git repository to your local directory
```sh
git clone https://gitlab.cs.duke.edu/cl619/vaccine-concern-classifier.git
```
2. Install Anaconda for your system (ideally macOS), navigate to the main directory, and create the `vax_trends` environment (Python 3.11.9)
```sh
conda env create -f environment_macos.yml
```
or use `environment_ubuntu.yml` for ubuntu systems.

3. To "install" our custom package, type `pwd` and add the path to PYTHONPATH using
```sh
export PYTHONPATH="${PYTHONPATH}:/my/working/directory"
```

4. To "install" the classifier, download the 2 files found in [drive](https://drive.google.com/drive/folders/1K3Z0O7WiFfPLeQfE5bbYcaVaANmfzJ68?usp=drive_link) and place them in the `demo/main/classifiers` directory.

5. Add a new directory `classifiers/` under the `demo/main/` directory, then place models weights there. It should look like:
```sh
.
├── demo
│   ├── main
│   │   ├── classifiers
│   │   │   ├── weighted_log1p_classifier.pth    # example
│   │   │   └── weighted_log1p_history.pkl
...
```

6. Run app `python demo/manage.py runserver`

7. Go to URL: `http://127.0.0.1:8000/about/`

## Repository structure
```sh
.
├── README.md
├── data/                       # all raw and processed data
├── demo                        # demo Django server
│   ├── demo
│   │   ├── settings.py         # server settings
│   ├── main                    # main app directory
│   │   ├── classifiers/        # classifiers used in the app
│   │   ├── static/             # all static assets
│   │   ├── templates/          # html files used for the app
│   │   ├── urls.py             # links to the templates
│   │   └── views.py            # primary logic for file processing
│   └── manage.py
├── multilabel_classification
│   ├── ann_with_GPT/           # uses GPT to annotate the dataset
│   └── bert_classifier/        # BERT model training scripts and evaluation
├── relevance_classifiers
│   ├── bert_classifier/        # bert classifier
│   ├── gpt_evaluation/         # GPT evaluation
│   ├── keyword_classifier/
│   └── xgboost_classifier/
└── vax_concerns_classifiers    # custom package for interacting with BERT models
    ├── multilabel_classifiers
    └── relevance_classifiers
```