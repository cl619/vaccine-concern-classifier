from vax_concerns_classifiers import MultilabelWeighted, CATEGORIES
import numpy as np
import pandas as pd
import os

LOCAL_DIR = os.path.dirname(os.path.abspath(__file__))
DATA_DIR = os.path.join(LOCAL_DIR, '..', '..', 'data')

full_df = pd.read_csv(os.path.join(DATA_DIR, 'full.csv'))
texts = full_df["paragraph_content"].to_list()

weighted = MultilabelWeighted()
weighted.load_model(os.path.join(LOCAL_DIR, '..', 'bert_classifier', 'scripts', 'weighted_log1p'), 'weighted_log1p')
labels = weighted.predict(texts)
# labels = [{cat:classif for cat, classif in zip(CATEGORIES, l)} for l in labels]

# full_df['bert_ann'] = labels
# full_df.to_csv(os.path.join(DATA_DIR, 'full_bert.csv'), index=False)