Please tell us about meningococcal disease.
Meningococcal disease is a bacterial infection caused by Neisseria meningitidis. Meningococcal disease usually presents clinically as meningitis (about 50% of cases), bacteremia (30% of cases), or bacteremic pneumonia (15% of cases). N. meningitidis colonizes mucosal surfaces of the nasopharynx and is transmitted through direct contact with large-droplet respiratory tract secretions from patients or asymptomatic carriers. Meningococcal disease can be severe. The overall case-fatality ratio in the U.S. is 15%, and 10%-20% of survivors have long-term sequelae such as neurologic disability, limb or digit loss, and hearing loss.
N. meningitidis is classified into 12 serogroups based on characteristics of the polysaccharide capsule. Most invasive disease (such as meningitis and sepsis) is caused by serogroups A, B, C, W, X and Y. The relative importance of serogroups depends on geographic location and other factors such as age. Serogroups B and C are the most frequent causes of disease in the U.S., accounting for 42% and 26% of cases with known serogroup, respectively, during 20152018. Serogroups W, Y, and nongroupable strains each caused 9%14% of cases during that period. Serogroup A is rare in the U.S. Historically, serogroup A was common in the meningitis belt of sub-Saharan Africa, but after the implementation of a meningococcal serogroup A conjugate vaccine campaign, serogroup A disease has been nearly eliminated in the meningitis belt.
Nasopharyngeal carriage rates are highest in adolescents and young adults who serve as reservoirs for transmission of N. meningitidis.
How common is meningococcal disease?
The incidence of meningococcal disease has declined steadily in the U.S. since a peak of reported disease in the late 1990s. Even before routine use of a meningococcal conjugate vaccine (MenACWY) in adolescents was recommended in 2005, the overall annual incidence of meningococcal disease had decreased 64%, from 1.1 cases per 100,000 population in 1996 to 0.4 cases per 100,000 population in 2005. In 2018, the rate of meningococcal disease in the U.S. reached a historic low of 0.1 cases per 100,000 population. Incidence of disease caused by serogroup B, a serogroup not included in the routinely recommended MenACWY vaccine, also has declined for reasons that are not known.
During 20152018, an estimated 360 cases of meningococcal disease occurred annually in the United States, representing an average annual incidence of 0.11 cases per 100,000 population. Of those with known serogroup in 2018 (N=302), 39% were serogroup B and 51% were serogroups C, Y, or W- 135. The incidence of disease is highest in infants under 1 year, children age 1 year, and adolescents age 1620 years.
What groups are at increased risk for meningococcal disease?
In addition to risk based on age, non-specific risk factors for serogroups A, C, W and Y include having a previous viral infection, living in a crowded household, having an underlying chronic illness, and being exposed to cigarette smoke (either directly or second-hand).
The following groups are at increased risk for all meningococcal serogroups:
•
People with persistent (genetic) complement component deficiencies (a type of immune system disorder)
•
People who use complement inhibitors such as eculizumab (Soliris, Alexion Pharmaceuticals) and ravulizumab (Ultomiris, Alexion Pharmaceuticals) for treatment of atypical hemolytic uremic syndrome or paroxysmal nocturnal hemoglobinuria
•
People with anatomic or functional asplenia
•
Microbiologists routinely exposed to meningococcal isolates in a laboratory
•
People at increased risk during an outbreak of meningococcal disease
•
Military recruits
•
College students
Certain groups are at increased risk of serogroups A, C, W and Y, but not serogroup B:
•
People living with HIV
•
Men who have sex with men (MSM)
•
Travelers to countries where meningococcal disease is endemic or hyperendemic, such as the meningitis belt of sub-Saharan Africa
What meningococcal vaccines are available in the United States?
The vaccines for meningococcal serogroups A, C, W, and Y (MenACWY; Menactra, Sanofi Pasteur; Menveo, GlaxoSmithKline [GSK]; MenQuadfi, Sanofi Pasteur) contain meningococcal conjugate in which the surface polysaccharide is chemically bonded ("conjugated") to a protein to produce a robust immune response to the polysaccharide. Although each of the 3 MenACWY vaccine products uses a different protein conjugate, the products are considered interchangeable; the same vaccine product is recommended, but not required, for all doses.
A discontinued meningococcal polysaccharide vaccine (MPSV4, Menomune, Sanofi Pasteur) was available in the United States until all doses expired in September 2017. With rare exception, it was not interchangeable with MenACWY conjugate vaccines.
Since late 2014, vaccines have become available that offer protection from meningococcal serogroup B disease (MenB; Bexsero, GSK; Trumenba, Pfizer). These vaccines are composed of proteins found on the surface of the bacteria. These vaccine products are not interchangeable; the same vaccine product is required for all doses.
MenACWY vaccines provide no protection against serogroup B disease, and meningococcal serogroup B vaccines (MenB) provide no protection against serogroup A, C, W, or Y disease. For protection against all 5 serogroups of meningococcus, it is necessary to receive both MenACWY and MenB.
Trade Name
Type of Vaccine
Serogroups
Year Licensed
Approved Ages
Menactra
Conjugate
A, C, W, Y
2005
9 mos.55 years*
Menveo
Conjugate
A, C, W, Y
2010
2 mos.55 years*
MenQuadfi
Conjugate
A, C, W, Y
2020
2 years and older
Trumenba
Protein
B
2014
1025 years*
Bexsero
Protein
B
2015
1025 years*
Where can I find the most current meningococcal vaccine recommendations?
Who is recommended to be vaccinated against meningococcal B disease?
MenB is routinely recommended for these groups:
•
People age 10 years and older who have functional or anatomic asplenia (including sickle cell disease)
•
People age 10 years and older who have persistent complement component deficiency (an immune system disorder) or who take a complement inhibitor such as eculizumab (Soliris, Alexion Pharmaceuticals) or ravulizumab (Ultomiris, Alexion Pharmaceuticals)
•
People age 10 years and older who are exposed during an outbreak caused by serogroup B
•
Microbiologists who work with meningococcal isolates in a laboratory
For adolescents and young adults not otherwise at increased risk for meningococcal B disease, ACIP recommends that a MenB series may be administered to people 16 through 23 years of age (preferred age 16 through 18 years) on the basis of shared clinical decision-making. The shared clinical decision-making recommendation allows the clinician and patient to decide together based upon the risks and benefits of vaccination for the individual patient.
ACIP has changed its MenB vaccine recommendation for adolescents not otherwise at increased risk of meningococcal disease from "Category B" to "shared clinical decision-making." What does this mean? Does the Affordable Care Act (ACA) require health plans (non-grandfathered) to provide benefit coverage on vaccines recommended for shared clinical decision-making?
The change from "Category B" to "shared clinical decision-making" was done to describe more clearly the intent of the recommendation that the patient should be informed of the option to be vaccinated against meningococcal serogroup B disease and that the decision to vaccinate against MenB should be made by the provider and patient together. ACA requires coverage of vaccines as indicated on the recommended immunization schedule, including vaccines with shared clinical decision-making recommendations. The Vaccines for Children (VFC) program also covers vaccines recommended for shared clinical decision-making.
What kind of information about MenB vaccine should be considered with a patient when conducting shared clinical decision-making?
To assist with the shared clinical decision-making around the option to vaccinate against meningococcal serogroup B disease and the timing of vaccination, CDC has provided some specific considerations about the disease and the vaccine that the patient and provider may weigh:
•
Serious nature of invasive meningococcal serogroup B infection, with a high risk of death and permanent complications
•
Low level of serogroup B disease in the United States, with an average of 34 cases each year among people age 16 through 23 years between 2015 and 2018
•
Increased risk among college students, especially those who are freshmen, attending a 4-year university, living in on-campus housing, or participating in sorority and fraternity life
•
Protection of MenB vaccine against most strains of meningococcal serogroup B bacteria
•
Estimated relatively short duration of MenB vaccine protection, with antibody levels waning within 12 years of completing the primary series
•
Evidence to date suggests no impact of MenB vaccine on meningococcal B carriage (may protect an individual from invasive disease but is unlikely to impact transmission of the bacteria to others)
The ACIP recommendations for meningococcal serogroup B (MenB) vaccine say the vaccine will provide "short term protection." What does "short term protection" mean?
MenB vaccines were approved based on the serologic response to the vaccine. No data are available on vaccine effectiveness against clinical disease or duration of protection against clinical disease. Short term protection refers to the known duration of the antibody response. Available data indicate that protective antibody levels wane in most recipients within 12 years of completion of the primary series.
Should college students be vaccinated against meningococcal serogroup B disease?
Several small outbreaks of meningococcal serogroup B disease have occurred on college campuses since 2013. Data derived from enhanced CDC meningococcal disease surveillance suggest that college students have 3.5 times the risk of meningococcal serogroup B disease relative to peers not attending college, although the disease incidence in college students remains very low (0.17 cases per 100,000 population). ACIP does not routinely recommend MenB vaccine for college students. However, the recommendation for shared clinical decision-making applies to all college students age 16 through 23 years who may choose to receive MenB vaccine to reduce their risk of meningococcal serogroup B disease. In addition, some colleges and universities require MenB vaccination for incoming students.
What is the schedule for MenB?
Trumenba (MenB-Fhbp, Pfizer) is either a 2-dose series with doses administered at least 6 months apart or a 3-dose series with the second and third doses administered 12 and 6 months after the first dose. Bexsero (MenB-4C, GSK) is a 2-dose series with doses given at least 1 month apart.
Which patients should receive a 2-dose schedule of Trumenba (MenB-FHbp)?
Healthy adolescents who are not at increased risk for meningococcal B disease should receive 2 doses of Trumenba (MenB-FHbp) administered at 0 and 6 months. If the second dose is given at an interval of less than 6 months, a third dose should be given at least 4 months after the 2nd dose.
For people age 10 years and older at increased risk for meningococcal B disease, 3 doses of Trumenba should be administered at 0, 12, and 6 months. The 3-dose series should be used for all people with functional or anatomic asplenia, people with persistent complement component deficiency (an immune system disorder) or those who take a complement inhibitor (eculizumab [Soliris] or ravulizumab [Ultomiris]), microbiologists who work with meningococcal isolates in a laboratory, and people exposed during serogroup B outbreaks.
A microbiologist in our facility received 2 doses of Trumenba (MenB-FHbp) 6 months apart. Does he need to receive a third dose?
No. The 3-dose series (at 0, 12 and 6 months) is intended to rapidly induce immunity to serogroup B meningococcal bacteria. If a microbiologist or other person at increased risk has received 2 doses of Trumenba separated by 6 months their vaccine series can be considered to be complete.
I have a patient who was given his first dose of Trumenba (MenB-FHbp) two weeks ago in May, but he just learned that his university requires all students to complete MenB vaccination before arriving on campus in September. His next dose of Trumenba is due in November. What can we do?
The only way to meet the school entry deadline for a complete series by September would be to restart a primary series with Bexsero (MenB-4C), which requires two doses spaced at least 4 weeks apart. You must wait a minimum of 4 weeks after the Trumenba dose to initiate the Bexsero series.
If a patient received Trumenba meningococcal B vaccine (MenB-FHbp) 2 months ago and Bexsero meningococcal B vaccine (MenB-4C) yesterday, should they complete the series with Trumenba or with Bexsero since the two brands are not interchangeable? What would be the intervals from the Bexsero dose to the subsequent dose(s)?
The patient can complete the series with either vaccine. If Bexsero (MenB-4C) is chosen, the second and final dose should be administered at least 1 month after yesterday’s dose. If Trumenba (MenB-FHbp) is chosen and the patient is healthy (i.e., does not have a high-risk condition for meningococcal B disease such as asplenia), the second and final dose of Trumenba should be administered at least 4 months after yesterday’s Bexsero dose (6 months after the first Trumenba dose). If the person is at increased risk for meningococcal B disease and Trumenba is being used, a second Trumenba dose should be administered 1 month after yesterday’s Bexsero dose and a third dose should be administered 4 months after the second Trumenba dose.
Can you provide a comprehensive overview of the MenB recommendations?
Are the two MenB vaccines interchangeable?
No. The ACIP meningococcal serogroup B vaccine recommendations state that the same vaccine must be used for all doses in the MenB series, including booster doses. If the brand of a previous dose is unavailable or cannot be determined, restart the primary series with the available brand.
Do any of the bacterial vaccines that are recommended for people with functional or anatomic asplenia need to be given before splenectomy? Do the doses count if they are given during the 2 weeks prior to surgery?
Pneumococcal conjugate vaccine (PCV13), Haemophilus influenzae type b (Hib) vaccine, meningococcal ACWY vaccine, and MenB vaccine should be given 14 or more days before splenectomy, if possible. Doses given during the 2 weeks (14 days) before surgery can be counted as valid. If the doses cannot be given prior to the splenectomy, they should be given as soon as the patient's condition has stabilized after surgery. Pneumococcal polysaccharide vaccine (PPSV23) should be administered 8 weeks after the dose of PCV13 for people 2 years of age and older.
Our practice has an 11-year-old patient who is having a splenectomy. The doctor requested meningococcal serogroup B vaccine (MenB) before the surgery and wants to know if the patient will need booster doses or a repeat MenB series at some point in the future (as in the meningococcal ACWY vaccine recommendations).
Yes. The 2020 ACIP recommendations for MenB include a booster dose schedule for MenB vaccination of people at high risk for meningococcal serogroup B disease. The first booster dose is recommended one year after completion of the primary series, with a subsequent booster dose administered every 23 years thereafter, as long as risk remains. Because MenB vaccine brands are not interchangeable, all doses, including booster doses, should be of the same MenB brand. If the brand of the primary series is not known or not available, CDC recommends restarting the primary series with the available product.
I have a patient with paroxysmal nocturnal hemoglobinuria who is being treated with Soliris (eculizumab). Should he receive meningococcal vaccine?
Eculizumab (Soliris) and the related long-acting compound, ravulizumab (Ultomiris) bind to C5 and inhibit the terminal complement pathway. People with persistent complement component deficiency due to an immune system disorder or use of a complement inhibitor are at increased risk for meningococcal disease even if fully vaccinated. This patient should be given a series of MenACWY vaccine, MenACWY (2 doses separated by at least 8 weeks) and a 2- or 3-dose series (depending on brand) of MenB vaccine. The patient should receive regular booster doses of MenACWY and MenB as long as he remains at risk: a booster dose of MenACWY every 5 years and a booster dose of MenB one year after completion of the primary series, followed by a booster dose of MenB every 23 years thereafter.
Because patients treated with complement inhibitors can develop invasive meningococcal disease despite vaccination, clinicians using Soliris or Ultomiris also may consider antimicrobial prophylaxis for the duration of complement inhibitor therapy.
We have a 10-year-old getting renal dialysis. The nephrologist will be starting her on ravulizumab (Ultomiris), which interferes with C5 complement. If we administer MenACWY and pneumococcal polysaccharide vaccine (PPSV23) now, and then give her PCV13 in 8 weeks, will the PCV13 interfere with the efficacy of the PPSV23 or the MenACWY?
Recommendations to separate MenACWY and PCV13 only apply to one of the three MenACWY vaccines, MenACWY-D (Menactra), and also only apply to individuals with functional or anatomic asplenia or HIV infection. So the best schedule is to give MenACWY (any brand) simultaneously with PCV13, and then PPSV23 in eight weeks. ACIP recommends giving PCV13 before PPSV23 in order to maximize the immune response from PCV13. PPSV23 may blunt the immune response to PCV13 if PCV13 is given after PPSV23, although in children there is a smaller effect than in adults. A 10 year-old with persistent complement component deficiency also should receive a 2- or 3-dose series (depending on brand) of MenB vaccine.
As long as the child remains at high risk of meningococcal disease due to complement inhibitor use, booster doses of both MenACWY and MenB are recommended. A MenACWY booster dose should be given every 5 years and a MenB booster dose should be given one year after the completion of the primary series, followed by a booster dose every 23 years thereafter.
Are people who are HIV-positive in a risk group for meningococcal disease?
People age 2 years and older with HIV infection who have not been previously vaccinated should receive a 2-dose primary series of MenACWY (doses separated by at least 8 weeks). People with HIV infection who have previously received one dose of MenACWY should receive a second dose at the earliest opportunity (at least 8 weeks after the previous dose) and then receive booster doses at the appropriate intervals. ACIP does not recommend routine meningococcal serogroup B vaccination of people with HIV infection: MenB may be given based upon shared clinical decision-making to people with HIV who are age 16 through 23 years old, preferably between ages 16 and 18 years.
If you choose to give Trumenba brand MenB vaccine (MenB-FHbp) to a 16-year-old with HIV infection based on shared clinical decision-making, should you use the 2-dose (standard) schedule or the 3-dose (high-risk) schedule?
Either Trumenba (MenB-FHbp) or the Bexsero MenB vaccine brand (MenB-4C) may be used for people with HIV infection. If Trumenba is administered, the CDC meningococcal subject matter experts recommend that the 3-dose schedule should be used. People with HIV infection do not appear to be at higher risk for meningococcal serogroup B disease, but because of their HIV infection they might not respond to the vaccine as well, the 3-dose schedule is preferred. When Bexsero is used, the schedule is 2 doses, regardless of risk status. Booster doses of MenB are not recommended for people with HIV in the absence of another indication for MenB vaccination.
I have a 7-year-old patient with congenital asplenia. She has already received PCV13 and meningococcal ACWY conjugate vaccine. Would you consider giving her meningococcal B vaccine (MenB)?
Use of either brand of MenB in persons younger than age 10 years is off-label in the U.S. There is no ACIP recommendation for use of this vaccine for this age group.
Are microbiologists recommended to receive MenB vaccine? And if so, how frequently?
ACIP recommends that microbiologists who work with meningococcal isolates in a laboratory receive both MenB and MenACWY vaccines. MenB can be given at the same time as any other vaccine. You can administer either two doses of Bexsero (MenB-4C) 4 weeks apart, or three doses of Trumenba (MenB-FHbp) on a 0-, 1–2-, and 6-month schedule.
Because protective antibody levels begin to wane within 12 years after completing the primary series, ACIP recommends a booster dose of MenB one year after completing the primary series, followed by a booster dose every 23 years thereafter, as long as risk remains. MenB vaccine brands work differently and are not interchangeable. All doses, including booster doses, should be of the same brand. If the brand of the primary series is not known or is not available, restart the primary series with the available brand.
We have a 19-year-old patient with a history of vasculitis, nephritis, and asthma. She is on azathioprine (Imuran) and is immunosuppressed. Her rheumatologist recommends she receive pneumococcal conjugate vaccine (PCV13, Prevnar 13, Pfizer) and MenB vaccine. How often should these vaccines be given? Will she require a series of PCV13 doses or just a booster?
MenB is not specifically recommended for immunosuppressed people. However, after discussing the pros and cons of vaccination (also known as shared clinical decision-making), people age 16 through 23 years who are not at increased risk may receive routine MenB vaccination with either a 2-dose series of Bexsero (MenB-4C) 4 weeks apart, or a 2-dose series of Trumenba (MenB-FHbp) 6 months apart.
I have a 10-year-old patient traveling to Kenya for one week. In addition to MenACWY vaccine, should she be offered meningococcal serogroup B (MenB) vaccine?
ACIP does not recommend routine MenB vaccination for travel to countries in sub-Saharan Africa or to other countries for which MenACWY vaccine is recommended. Meningococcal disease in these areas is generally not caused by serogroup B.
Which groups should receive a booster dose of MenB?
ACIP recommends booster doses of MenB vaccines for people at increased risk of MenB disease. Booster doses should be administered to people in the following groups as long as increased risk remains:
•
People with functional or anatomic asplenia, including sickle cell disease
•
People with persistent complement component deficiency (an immune system disorder)
•
People who take a complement inhibitor (eculizumab [Soliris] or ravulizumab [Ultomiris])
•
Microbiologists who routinely work with meningococcal isolates
•
Previously vaccinated people who are at risk during a meningococcal B disease outbreak
Because protective antibody levels produced by the primary series begin to wane within 12 years, the first booster dose is recommended one year after completion of the primary series, with subsequent booster doses every 23 years as long as increased risk remains. Previously vaccinated people identified by public health as being at risk during a meningococcal B outbreak should receive a booster dose if it has been at least one year since completion of their primary series, though depending upon the specific circumstances, public health may recommend a booster dose as little as 6 months after completion of the primary series.
I have a 28 year-old patient who received a primary MenB vaccine series of Bexsero in 2015 when her spleen was removed. At that time, the ACIP did not have a recommendation for booster doses. Do I need to give her a new primary series?
Brands of MenB vaccine work differently and are not interchangeable. The only time ACIP recommends restarting the primary series is if the brand used for the primary series is not known or is unavailable.
Public health authorities have declared a meningococcal serogroup B disease outbreak at my university and we are now vaccinating all students on campus. Some students report having had a primary series of MenB vaccine, but do not have documentation of which brand was used. What should we do?
During an outbreak of meningococcal B disease, swift protection of those at risk is prioritized and CDC subject matter experts do not recommend delaying vaccination in order to locate records. Student health services with documentation of MenB vaccination (including brand) of incoming students, either in a state immunization registry or in student health records, will be able to respond most efficiently to an outbreak.
Students whose primary series of MenB vaccine was completed at least 1 year before the outbreak (or as little as 6 months before the outbreak, if recommended by public health) should receive a single booster dose of the same brand of MenB vaccine. If the same brand is unavailable, they should restart the primary series with the available brand. If the brand of the primary series is unknown, administer a dose of the available product and counsel the recipient to request records of the primary series: if the primary series brand is different, then in order to ensure optimal protection, the recipient should be given a booster dose of the primary series product or complete a primary series with the available product after a minimum interval of 4 weeks.
I know the primary series of MenB vaccine should use the same brand for all doses. Does that also apply to booster doses?
Yes. MenB vaccines work differently and receiving mismatched MenB doses might result in inadequate protection. For this reason, documentation of the brand of vaccine in immunization is especially important. If a patient at high risk requires a booster dose and the brand of the primary series doses cannot be determined or is unavailable, then CDC recommends restarting the primary series with the available brand.
The first booster dose is recommended one year following completion of the primary series with subsequent booster doses every 23 years thereafter, as long as risk remains.
By what route should meningococcal vaccines be administered?
All meningococcal vaccines available in the United States, including MenACWY and MenB are given by the intramuscular route.
Can MenACWY and MenB vaccines be given at the same visit?
Yes. MenACWY and MenB vaccines can be given at the same visit or at any time before or after the other.
What adverse events are expected after receiving MenB?
In clinical trials and in postlicensure safety surveillance, the most common local adverse events within 7 days of receiving MenB were injection site pain, swelling or redness and the most common systemic symptoms were headache, fatigue and body aches. In general, these types of self-limited reactions are reported more frequently than with MenACWY vaccination.
What are the contraindications and precautions for MenB?
As with all vaccines, a severe allergic reaction (for example, anaphylaxis) to a vaccine component or to a prior dose is a contraindication to further doses of that vaccine. The tip caps of the Bexsero pre-filled syringes contain natural rubber latex which may cause allergic reactions in latex sensitive individuals. A moderate or severe acute illness is a precaution; vaccination should be deferred until the person’s condition has improved. Because MenB is an inactivated vaccine it can be administered to persons who are immunosuppressed as a result of disease or medications; however, response to the vaccine might be less than optimal. Data on MenB vaccination during pregnancy is limited. Pregnancy a precaution to MenB vaccination, but MenB may be administered if, in the judgment of the clinician, the benefits outweigh any potential risks.
Should a pregnant woman receive MenB vaccine?
Few data are available on the effect of MenB vaccines on pregnancy. The manufacturers do not consider pregnancy to be a contraindication to use of MenB. GSK has established a Vaccination in Pregnancy registry. Women who receive Bexsero during pregnancy are encouraged to participate in the registry by calling 877-683-4732. Pfizer also maintains a Vaccination in Pregnancy registry for Trumenba, although specific contact details for this registry are not available. No data are available from these registries. In general, vaccination against MenB should be deferred during pregnancy; however, MenB may be administered if, in the judgment of the clinician, the benefits outweigh any potential risk.
What is the storage requirements for MenB?
Store at MenB at refrigerator temperature, between 2° and 8°C (between 36° and 46°F). The vaccines must not be frozen. Vaccine that has been frozen or exposed to freezing temperature should not be used. Do not use after the expiration date.
