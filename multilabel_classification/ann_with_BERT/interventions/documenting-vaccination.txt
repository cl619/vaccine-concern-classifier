In the past, CDC has recommended to only accept patient-reported history for influenza and pneumococcal polysaccharide vaccines. Is the recommendation to not accept a patient-reported history of pneumococcal conjugate vaccine?
Where can I find a list of vaccines currently licensed for use in the U.S.?
When we are giving multiple injections in a limb, what is the best way to accurately document the injection site?
One way to handle this is to indicate if the vaccination was given either in the "upper" or "lower" portion of the injection area selected (e.g., DTaP: right thigh, upper; Hib: right thigh, lower; or PCV13: left thigh, upper; HepB: left thigh, lower). It is helpful if everyone in your office or clinic uses the same sites for each vaccine. Use of a standardized site map can facilitate this. Here are some helpful site maps for different ages so you can record where shots were given:
We frequently see patients, such as immigrants, who do not have records of past vaccination or who insist they or their children are up to date. Should we accept their undocumented vaccination history?
Vaccination providers frequently encounter people who do not have adequate documentation of vaccinations. Providers should only accept written, dated records as evidence of vaccination. With the exception of influenza and pneumococcal polysaccharide vaccines, self-reported doses of vaccine without written documentation should not be accepted. An attempt to locate missing records should be made whenever possible by contacting previous healthcare providers, reviewing state or local immunization information systems, and searching for a personally held record. However, if records cannot be located or will definitely not be available anywhere because of the patient's circumstances, people without adequate documentation should be considered susceptible and should be started on the age-appropriate vaccination schedule. Serologic testing for immunity is an alternative to vaccination for certain antigens (e.g., measles, rubella, or hepatitis A).
We sometimes encounter patients with foreign vaccination records. We suspect that some of these records are not valid. What should we do?
Where can I find names of vaccines used outside the U.S.?
Since the Health Insurance Portability and Accountability Act (HIPAA) went into effect, we are unsure if we can share immunization information on our pediatric patients with staff in schools or daycare facilities.
Healthcare providers (or other covered entities) may share immunization information with schools or daycare facilities, without authorization, if permitted or required by state law. These state laws would not be preempted by the HIPAA Privacy Rule [45 CFR 160.203(c)].
If a patient or parent refuses recommended vaccinations, is it necessary for them to sign a refusal form, or is the provider's documentation sufficient?
How can I find out if our state or locality has an Immunization Information System (IIS), or registry, in which I might participate?
If my state has an Immunization Information System (IIS, or registry) do I still need to give the patient a vaccine record card?
Yes. Patient-held cards are an extremely important part of a person's medical history. The person may move to an area without a registry, and the personal record may be the only vaccination record available. In addition, even within a state, all healthcare providers may not participate in the registry, and the personal record card would be needed.
Please explain the federal requirements for all healthcare providers that administer vaccines under the National Childhood Vaccine Injury Act.
The National Childhood Vaccine Injury Act (NCVIA), enacted in 1986, set forth 3 basic requirements for all vaccination providers, which are:
•
Providers must give the patient (or parent/legal representative of a minor) a copy of the relevant federal "Vaccine Information Statement" (VIS) for the vaccine they are about to receive.
•
Providers must record certain information about the vaccine(s) administered in the patient's medical record or a permanent office log (see next question).
•
Providers must document any adverse event following the vaccination that the patient experiences and that becomes known to the provider, whether or not it is felt to be caused by the vaccine, and submit the report to the Vaccine Adverse Event Reporting System (VAERS).
What do we legally need to record when giving an immunization to a patient?
It is important to know the federal requirements for documenting the vaccines administered to your patients. The requirements are defined in the National Childhood Vaccine Injury Act enacted in 1986. The law applies to all routinely recommended childhood vaccines, regardless of the age of the patient receiving the vaccines. The only vaccines not included in this law are pneumococcal polysaccharide, zoster, and certain infrequently used vaccines, such as rabies and Japanese encephalitis.
The following information must be documented on the patient's paper or electronic medical record or on a permanent office log:
1.
The vaccine manufacturer.
2.
The lot number of the vaccine.
3.
The date the vaccine is administered.
4.
The name, office address, and title of the healthcare provider administering the vaccine.
5.
The Vaccine Information Statement (VIS) edition date located in the lower right corner on the back of the VIS. When administering combination vaccines, all applicable VISs should be given and the individual VIS edition dates recorded.
6.
The date the VIS is given to the patient, parent, or guardian.
The federally required information should be both permanent and accessible.
Federal law does not require a parent, patient, or guardian to sign a consent form in order to receive a vaccination; providing them with the appropriate VIS(s) and answering their questions is sufficient under federal law.
Which vaccines are covered by NCVIA?
NCVIA requirements apply to diphtheria, tetanus, pertussis, measles, mumps, rubella, polio, hepatitis A, hepatitis B, Haemophilus influenzae type b (Hib), varicella, seasonal influenza (inactivated and live attenuated), pneumococcal conjugate, meningococcal, rotavirus, and human papillomavirus (HPV) vaccine.
When and to whom is it required to give Vaccine Information Statements (VISs)?
Where can I get instructions on how, why, and when to use the federally-mandated VISs?
When are VISs released for new vaccines?
An interim VIS can sometimes be released soon after licensure of the vaccine or after the official vote by ACIP is taken. The interim VIS is not replaced with a final version until the ACIP recommendations have been published and the new VIS has been developed according to legally-mandated procedures.
Is it required to use a VIS in an emergency room when we give Td/Tdap to a patient?
Yes. The National Childhood Vaccine Injury Act requires that a VIS be given to people of any age before they receive a dose of any vaccine included in the Act. Tetanus and diphtheria toxoids (and pertussis vaccine) are included in the Act. If the patient is unaccompanied and unable to clearly read and understand the information in the VIS (e.g., the patient is unconscious), this should be noted in the patient's chart.
Our large pediatric practice is struggling with the requirement to provide VISs to the parents of every child we vaccinate. We would like to create a re-usable packet of laminated VIS sheets (fastened together on a ring). We plan to place a packet in each exam room for parents to read prior to vaccine administration. On the bottom of each sheet would be a statement, "If you would like a copy of this sheet to take home, please ask our staff." This will ensure that parents are given the VIS sheets to read prior to vaccine administration. It will also help save paper. Our experience is that many parents throw out the VIS documents or leave them behind in the waiting room. Is this an acceptable procedure?
Your solution will meet the spirit of the federal law, as long as you make sure to encourage the patient (or parent) to take home a paper copy of the VIS and to refer to it if needed (e.g., if they need to know what to do if there is an adverse event or how to contact VAERS).
We operate an acute care hospital and commonly give vaccinations to our employees and patients, including our annual staff influenza vaccination campaign. Are we required to use VISs in all of these settings, or does that apply only to patients seen in outpatient settings?
VISs must be given, regardless of the setting, to all people, including adults, before administering HPV, Td, Tdap, MMR, varicella, hepatitis A, hepatitis B, meningococcal, influenza, or polio vaccine.
When using VISs and providing vaccines, is a parent/guardian signature required?
Where can I get VISs for some of the newer combination vaccines?
CDC currently has no plans to develop VISs for Pediarix, Twinrix, Kinrix, Quadracel, or Pentacel. When administering these combination vaccines, use the VISs for all component vaccines. For certain combination vaccines given to children, you can use the multi-vaccine VIS (which includes DTaP, Hib, HepB, polio, and PCV13) and check the appropriate box(es), just as you would if you were administering the individual vaccines. If the multi-vaccine VIS is unavailable, you should use the individual vaccine VISs. A VIS was developed for MMRV vaccine because of its unique adverse reaction profile.
It seems CDC is changing the format of VISs. Do we have to throw our old supply away and use the new ones?
Why does CDC include 2D barcodes on VISs?
CDC began adding barcodes to VISs in 2012. The barcode is intended to save time and prevent documentation errors by allowing immunization providers to scan the name and edition date of the VIS, information required to be documented in the permanent record of immunization, into an electronic medical record, immunization information system, or other electronic database. Scanning the barcode instead of manually recording the information is optional but can be helpful.
Where can I get foreign language VISs?
