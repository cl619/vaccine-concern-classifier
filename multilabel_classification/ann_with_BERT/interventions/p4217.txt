immunization
action coalition

A

immunize.org

Rotavirus: Questions and Answers

INFORMATION ABOUT THE DISEASE AND VACCINES

What causes rotavirus disease?

Rotavirus disease is caused by a virus, the rotavi-
rus. The name rotavirus is derived from the Latin
rota, meaning “wheel,” because the rotavirus has a
wheel-like appearance when viewed by an electron
microscope.

How does rotavirus spread?

The rotavirus enters the body through the mouth
and then infects the lining of the intestines. Rotavi-
tus is very contagious, spreading easily from chil-
dren who are already infected to other children and
sometimes adults. Large amounts of rotavirus are
shed in the stool of infected people and the virus
can be easily spread via contaminated hands and
objects, such as toys. Children can spread rotavirus
both before and after they become sick with diar-
rhea. Rotavirus is very stable and may remain viable
in the environment for months if not disinfected.

How long does it take to show signs of
rotavirus after being exposed?

The incubation period for rotavirus diarrhea is 1 to
3 days. Symptoms of infection vary and depend on
whether it is the first infection or a repeat infection.

What are the symptoms of rotavirus?

In young children, rotavirus disease commonly
begins with fever and vorniting, followed by diar-
thea. Vomiting and diarrhea may last from three to
seven days. The diarrhea may be watery and may
lead to dehydration.

How serious is rotavirus?

All three symptoms of rotavirus disease (fever, vomn-
iting, and diarrhea} cause children to lose fluids.
Vomiting is especially dangerous because it's dif-
ficult to replace fluids in children who are vomiting
persistently.

Prior to the availability of rotavirus vaccine, rotavirus
infection was responsible for more than 400,000
doctor visits, more than 200,000 emergency room

visits, 55,000 to 70,000 hospitalizations, and 20 to
60 deaths in the United States each year. In the first
five years of life, four of five children in the United
States would develop rotavirus gastroenteritis, one
in seven would require a clinic or emergency room
visit, one in 70 would be hospitalized, and one in
200,000 would die from this disease.

In developing countries, rotavirus causes more than
500,000 deaths each year in children younger than
age five years.

What are possible complications from
rotavirus?

Rotavirus infection in infants and young children
can lead to severe diarrhea and dehydration. The
dehydration may be severe. | mmunodeficient chil-
dren may have more severe or persistent disease.

How do | know if my child has rotavirus?

Rotavirus disease is difficult to differentiate from
diarrheal illness caused by other pathogens. As a
result, laboratory testing of the stool is needed to
confirm a diarrheal illness as rotavirus disease.

Is there a treatment for rotavirus?

Children are typically treated by replacing lost body
fluids through drinking liquids specifically made for
rehydration; these liquids are called oral rehydration
solutions. These products contain specific amounts
of water, sugars, and salts. In severe cases, body
fluids are replaced with fluids given directly through
the veins by use of an intravenous line in the hospi-
tal.

How long is a person with rotavirus
contagious?

Infected persons shed large quantities of virus in
their stool beginning 2 days before the onset of diar-
rhea and for up to 10 days after onset of symptoms.
Rotavirus may be detected in the stool of persons
with immune deficiency for more than 30 days after
infection.

CONTINUED ON THE NEXT PAGE D>

Saint Paul, Minnesota - 651-647-9009 - www.immunize.org - wwwyaccineinformation.org

www. immunize.org/eatg.d/p4217.pdF Item #P4217 (1/19)
Rotavirus: Questions and Answers (continued)

page 2 of 3

 

Can you get rotavirus more than once?

A person may develop rotavirus disease more than
once because there are many different rotavirus
types, but second infections tend to be less severe
than the first infections. After a single natural infec-
tion, 40% of children are protected against a sub-
sequent rotavirus illness. Persons of all ages can get
repeated rotavirus infections, but symptoms may be
mild or not occur at all in repeat infections.

Wouldn't good hygiene be enough to prevent
rotavirus disease?

Better hygiene and sanitation have not been very
effective in reducing rotavirus disease. This is illus-
trated by the fact that virtually everyone in the world
is infected by rotavirus disease by age five years,
despite differences in sanitation between countries.

Can adults be infected with rotavirus?

Yes. Rotavirus infection of adults is usually asymp-
tomatic but may cause diarrheal illness. Outbreaks
of diarrheal illness caused by rotavirus have been
reported, especially among elderly persons living in
retirement communities.

When did a rotavirus vaccine become available?

A vaccine to prevent rotavirus gastroenteritis was
first licensed in 1998 but was withdrawn in 1999
because of its association with an uncommon type
of bowel obstruction called “intussusception.”

In 2006, the U.S. Food and Drug Administration
(FDA) approved a new rotavirus vaccine, RotaTeq
(by Merck). In 2008, FDA approved a second rotavi-
rus vaccine, Rotarix (by GlaxoSmithKline).

What kind of vaccine are they?
RotaTeq and Rotarix are both live attenuated (weak-

ened) viral vaccines.

How is this vaccine given?

Both RotaTeq and Rotarix are given to babies orally.

Who should get this vaccine?

National experts on immunization (such as the

Centers for Disease Control and Prevention and the
American Academy of Pediatrics} recommend rou-
tine vaccination of all infants with rotavirus vaccine.

What is the recommended schedule for getting
this vaccine?

Both vaccines require multiple doses. RotaTeq vac-
cine is given in a 3-dose series with doses at ages 2,
4, and 6 months; Rotarix vaccine is given in a 2-dose
series with doses at ages 2 and 4 months.

The first dose of either vaccine can be given as early
as age 6 weeks or as late as age 14 weeks, 6 days.
Vaccination should not be started for infants once
they reach their 15 week birthday. There must be at
least 4 weeks between doses and all doses must be
given by age 8 months. Rotavirus vaccine may be
given at the same time as other childhood vaccines.

Should an infant who has already been infected
with rotavirus still be vaccinated?

Yes. Infants who have recovered from a rotavirus
infection may not be immune to all of the virus types
present in the vaccine. So infants who have previ-
ously had rotavirus disease should still complete the
vaccine series if they can do so by age 8 months.

How safe is this vaccine?

Before being licensed by the Food and Drug Admin-
istration both rotavirus vaccines were studied in
clinical trials involving more than 60,000 infants.
Adverse reactions reported among vaccinated
infants in the trials included vomiting, diarrhea, irri-
tability and fever. However, children who received a
placebo developed the same symptoms at a similar
rate. No serious adverse reactions were identified in
the prelicensure trials.

The prelicensure clinical trials of both RotaTeq and
Rotarix did not find an increased risk for intussus-
ception (a type of bowel obstruction) among vac-
cine recipients. A large postlicensure study of more
than 1.2 million rotavirus vaccine recipients found
a very small increased risk of intussusception (1 to
1.5 additional cases of intussusception per 100,000
vaccinated infants) in the 7 to 21 days following the
first dose. No increased risk of intussusception was

CONTINUED ON THE NEXT PAGE D>

IMMUNIZATION ACTION COALITION - Saint Paul, Minnesota - 651-647-9009 - www.immunize.org - www.vaccineinformation.org

www. immunize.org/eatg.d/p4217.pdF Item #P4217 (1/19)
Rotavirus: Questions and Answers (continued)

page 3 of 3

 

found after the second or third doses. A study con-
ducted by the CDC Vaccine Safety Datalink found no
increased risk of intussusception following RotaTeq
but found an increased risk following the first and
second doses of Rotarix. Based on this study, one
case of intussusception would be expected for
approximately each 20,000 children, who are fully
vaccinated.

CDC and the Food and Drug Administration (FDA)
continue to believe that the benefits of rotavirus vac-
cination outweigh the risks associated with vaccina-
tion and that routine vaccination of infants should
continue.

How effective is rotavirus vaccine?

Rotavirus vaccine is very effective against rotavirus
disease. Studies show the vaccine to be highly effec-
tive (85% to 98%) against severe rotavirus disease
and effective against rotavirus disease of any sever-
ity (74% to 87%) through approximately the first
rotavirus season after vaccination. Chances that
children will need to be hospitalized for rotavirus
disease are also greatly decreased (96%) by the vac-
cine. Neither vaccine will prevent diarrhea or vomit-
ing caused by other germs.

Who should not receive rotavirus vaccine?

Any child who has had a severe (life-threatening)
allergic reaction to a previous dose of rotavirus vac-

cine should not get another dose. A child with a
severe (life-threatening) allergy to any component
of rotavirus vaccine should not get the vaccine.
Because the oral applicator for Rotarix contains
latex rubber, infants with a severe (anaphylactic)
allergy to latex should not be given Rotarix; the
RotaTeq dosing tube is latex-free. Rotavirus vac-
cine should not be given to an infant diagnosed
with the rare genetic disorder severe combined
immune deficiency (SCID). Infants who have had
intussusception are more likely to develop it again
compared to infants who have never had intussus-
ception. As noted above the first dose of rotavirus
vaccine has been associated with a small increased
risk of intussusception (1 to 1.5 cases per 100,000
first doses). So rotavirus vaccine should not be
given to an infant with a previous history of
intussusception.

Children who are moderately or severely ill at the
time the vaccination is scheduled should probably
wait until they recover, including children who are
experiencing diarrhea or vomiting. Healthcare pro-
viders will decide on a case-by-case basis whether to
vaccinate a child with an ongoing digestive problem,
an immune system weakened because of HIV/AIDS
or another disease that affects the immune system,
ora child who is receiving treatment with drugs
such as long-term steroids or treatment for cancer.

IMMUNIZATION ACTION COALITION - Saint Paul, Minnesota - 651-647-9009 - www.immunize.org - www.vaccineinformation.org

wwnw.immunize.org/eatg.d/p4217.pdf Item #P4217 (1/19)
