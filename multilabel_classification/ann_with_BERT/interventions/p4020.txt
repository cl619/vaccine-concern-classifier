Vaccinations for Preteens and Teens, Age 11-19 Years

Getting immunized is a lifelong, life-protecting job. Make sure you and your healthcare
provider keep your immunizations up to date. Check to be sure you've had all the vaccinations
you need.

 

Vaccine

Do you need it?

 

Chickenpox
(varicella; Var}

Yes! If you haven’t been vaccinated and haven't had chickenpox, you need 2 doses of this vaccine.
Anybody who was vaccinated with only 1 dose should get a second dose.

 

 

 

enzae type b (Hib)

Hepatitis A Yes! If you haven't been vaccinated, you need 2 doses of this vaccine. Anybody who was vaccinated
(HepA) with only 1 dose should get a second dose at least 6-18 months later.

Hepatitis B Yes! This vaccine is recommended for all people age 0-18 years. You need a hepatitis B vaccine series
(HepB) if you have not already received it.

Haemophilus influe | Maybe. \f you haven't been vaccinated against Hib and have a high-risk condition (such as a non-

functioning spleen), you need this vaccine.

 

Human
papillomavirus
(HPV)

Yes! All preteens need 2 doses of HPV vaccine at age 11 or 12. Older teens who haven't been vacci-
nated will need 2 or 3 doses. This vaccine protects against HPV, a common cause of genital warts and
several types of cancer, including cervical cancer and cancer of the anus, penis, and throat.

 

Influenza
(Flu)

Yes! Everyone age 6 months and older needs annual influenza vaccination every fall or winter and
for the rest of their lives.

 

Measles, mumps,
rubella (MMR)

Yes! You need 2 doses of MMR vaccine if you have not already received them. MMR vaccine is
usually given in childhood.

 

Meningococcal
ACWY
(MenACWY,
MCV4)

Yes! All preteens and teens need 2 doses of MenACWY vaccine, the first at age 11-12 years and the
second at age 16 years. If you are a first-year college student living in a residence hall, you need a
dose of MenACWY if you never received it or received it when you were younger than 16. If you have
a high-risk health condition, you will also need boosters if your risk is ongoing. Ask your healthcare
provider if you have a risk factor.

 

Meningococcal

Yes! Teens who want to be protected from meningitis type B are recommended to receive 2 doses of

 

PPSV23; Prevnar,
PCV13)

B (MenB) MenB vaccine starting at age 16. Teens with certain risk conditions (such as a non-functioning spleen)
should be vaccinated also, and receive boosters if their risk is ongoing. Ask your healthcare provider
if you have a risk factor.

Pneumococcal Maybe. Do you have an ongoing health condition? If so, check with your healthcare provider to find

(Pneumovax, out if you need one or both of the pneumococcal vaccines.

 

 

 

 

Polio Yes! You need a series of at least 3 doses of polio vaccine if you have not already received them.
(IPV} Polio vaccine is usually given in childhood.

Tetanus, Yes! All preteens and teens (and adults!) need a dose of Tdap vaccine, a vaccine that protects you
diphtheria, and | from tetanus, diphtheria, and whooping cough (pertussis). After getting a dose of Tdap, you will
whooping cough | need a Tdap or tetanus-diphtheria (Td) shot every ten years. If you become pregnant, you will need
(Tdap; Td) another dose of Tdap during every pregnancy, preferably during the third trimester.

 

immunization
action coalition

Will you be traveling outside the United States? Visit the Centers for Disease Control and Prevention’s
(CDC) website at wwwnc.cdc.gov/travel/destinations/list for travel information, or consult a travel clinic.

A Saint Paul, Minnesota + 651-647-9009 - www.immunize.org « www.vaccineinformation.org

immunize.org

www.immunize.org/catg.d/p4020.pdf « Item #P4020 (5/20)

 
