To Err ls Human:
Not to Err Is Better!

Vaccination Errors and How to Prevent Them

 

Types of vaccination errors

* Storage and handling
* Administration
* Scheduling

* Documentation

 

 

Vaccine storage and handling

* Vaccines are fragile and must
be kept at recommended
temperatures at all times

+ Vaccines are expensive

 

[tis better to NOT VACCINATE than to administer a
dose of vaccine that has been mishandled

coe

 

 

 

The results of storage and handling errors
* Your patients may get seriously ill from not being immune
from a vaccine-preventable disease

* You must revaccinate anyone who received a dose of
compromised vaccine

* You will have to explain to parents why their children must
repeat vaccine doses

* Your practice may experience negative publicity

* You may lose a lot of money

coc

 

 

 

 

 

Newspaper Headlines

 

Is any publicity really good publicity?

 

 

 

Immunization Action Coalition * (651) 647-3009 * www.immunize.org

“4,900 doses of lu vaccine spall in hospitals faulty fridge"
Wests YA; 1808

“Kaiser mishandles flu vaccine’
Fresna, CA; 128104

"Storage errors cause thousands to be vaccinated again’
Koon, TN: 1205

“U.S, doctor accused of giving last year's flu vaccine”
Betingham, WA; 1614

“Frozen vaccine could cost state more than $30,000"
Akanas; 1104

 

 

   

www.immunize.org/catg.d/s8020.pdf « Item #58020 (12/19)
From our IAC email archive...

 

 

 

HELP! “We have a local practice that had issues with
their refrigerator temperatures being too cold for an
extended period. All the vaccines that were given
during that time frame are now considered invalid.
They have many 2-year-old patients who received 4
doses of DTAP all of which were stored improperly...”

  

 

 

 

 

How to avoid storage

& handling problems M
+ Assign a vaccine manager
+ Store all vaccines appropriately

+ If using temperature monitoring device {TMD} that records min/max
temps, document min/max once each workday (preferably in
morning}; TMD does not record min/max temps, document
current temps twice (at beginning and end of workday}

+ Use only certified calibrated TMDs that use an active display to
provide continuous monitoring information

+ Maintain temp logs for 3 years
+ Implement a vaccine emergency system
+ Take immediate action for out-of-range temps

+ Do not store anything else in the refrigerator or freezer
Adapted cOC

 

 

Vaccine handling basics

* Open only one vial ata time

* Store vaccine vials separate from other medications or
biologics

Do NOT store food/beverages in refrigerator or freezer
with vaccines

Keep light-sensitive vaccines in their box until ready to use

Rotate your stocks so vaccine never become outdated

 

 

 

 

 

 

HELP! “Can you advise as to the safety and efficacy of
drawing up flu vaccine ahead of time for mass
vaccination clinics? One place | work is using vaccines
drawn from a multidose vial as much as a week
before actually giving the vaccine.”

 

 

 

Immunization Action Coalition * (651) 647-3009 * www.immunize.org

Prefilling syringes

* This practice is strongly discouraged by CDC

* May result in vaccine administration errors

* May consider in situations of heavy use of a single vaccine
{eg., annual influenza clinic}

* Consider using manufacturer-supplied prefilled syringes

* Syringes other than those filled by manufacturer should be
discarded at end of clinic day. Also, manufactured prefilled
syringes that have had the caps removed and a needle
attached to the syringe should be discarded at the end of
the day.

 

 

 

Adapted COC

 

www.immunize.org/catg.d/s8020.pdf « Item #58020 (12/19)
Live virus vaccines and some inactivated vaccines
must be administered promptly after reconstitution...

If not administered within the time limit, these
vaccinations need to be repeated!
Uf live virus vaccine, there is a 4-week minimum interval}

 

Examples of time limits for using vaccines
after reconstitution

Varicella <30 mins (and protect from light)
MMRV <30 mins (and protect from light)
Yellow fever <1 hour

MMR <8 hours fand protect from light)

Shingrix <6 hrs (and protect from light}

Menveo <8 hrs (and protect from light)

More information on vaccines with diluents and time limits:
wwrw.immunize.org/catg.d/p3040.pdi

 

 

Types of vaccination errors

Storage and handling

Administration

Scheduling

Documentation

 

 

 

Administering vaccines correctly

+ Ensure staff is adequately trained

+ Provide current immunization
education

 

+ Adhere to OSHA guidelines for
employee safety

+ Provide staff with easy-to-use
resources and guidelines

 

 

 

Adapted COC

 

Types of administration errors

+ Wrong vaccine or diluent

Wrong dosage

Expired vaccine

Incorrect route/site/needle size

 

 

 

Immunization Action Coalition * (651) 647-3009 * www.immunize.org

A study using the largest medication error reporting
database in the U.S. found that administration of the
wrong vaccine was commonly reported.

Such errors usually involved vaccines whose
generic or trade names looked or sounded alike
{Tdap/DTaP, Adacel and Daptacel}, or which
have similar packaging.

 

reece poosprsen-e AL

 

 

 

www.immunize.org/catg.d/s8020.pdf Item #58020 (12/19)
Similar packaging —
Check the vial 3 TIMES

Td

 

Tdap

 

Tdap or DTaP

Isit
Tdap,
DTap,
ora
combo?

Check
the vial
3 times!

  

 

 

Another source of confusion:
Varicella-containing vaccines

 

varivan
12 mos of age and older

 

eostavan
60 yrs of age and older

  

Preduad MMR
12 mos thew 12 yes
¥ Adapted CDC A =

 

 

 

More confusion: influenza vaccines

FUL" SAQULT INFLUENZA WACONE

 

 

http:/foae-orgfassetsidocs/ MIMI B38.pd

 

 

 

 

 

HELP! “A 63-year-old patient received a
high dose flu vaccine. Would the
recommendations be to re-vaccinate with
the standard dose, or are they considered
covered?”
ANSWER
Although this is a vaccine administration error,
according to COC, the dase can count. In general,
if a person receives a dose that is too large and/or
licensed for an older age group, the dose should
be considered valid.

 

 

Immunization Action Coalition + (651) 647-9009 * www.immunize.org

 

AVOID ERRORS

Check the vial 3 TIMES!!!

 

 

 

www.immunize.org/catg.d/s8020.pdf © Item #58020 (12/19)
Another potential problem...
Using the wrong diluent

ACtHIB » 0.4% sodium chloride RabAvert 1 sterile water

Hiberix « 0.9% sodium chloride Rotarix » sterile water,
calcium carbonate, & xanthan
Imovax « sterile water
Shingrix + ASD1, adiuvant
MMR, Varivax, ProQuad

Zostavax | sterile water YE-VAX «0.95 sodium chloride

 

HELP! “One of the nursing staff used the
Merck sterile water diluent to reconstitute
the ActHib instead of the 0.4% sodium
chloride that comes with it. Does it need to
be repeated, or will it be considered OK?”

ANSWER
If the wrong diluent is used, the immunization needs
to be repeated (except in the case of mixing up the
diluent between MMR, MMRY, Varivax, and Zostavax,
which are all made by Merck and use the same sterile
water diluent).

 

  

 

 

If an INACTIVATED vaccine is reconstituted with the
wrong diluent and is administered, the dose is invalid
and should be repeated ASAP.

If a LIVE vaccine is reconstituted with the wrong
diluent and is administered, the dose is invalid and if
it can’t be repeated on the same clinic day, it needs
to be repeated no earlier than four weeks after the
invalid dose. This spacing is due to the effects of
generating a partial immune response that could
suppress the live replication of subsequent doses,
even of the same live vaccine.

 

 

 

Related error: giving diluent only

The liquid diluents for Menveo (MenACWY} and
Pentacel (DTap-IPV/Hib) contain vaccine and need to
be combined with the lyophilized vaccine (powder)
to provide all the components.

  

One more caveat: the liquid diluent portion of the
Shingrix vaccine does not contain any antigen, but it
does include an adjuvant. Because of this, the CDC
experts recommend waiting 4 weeks for another
dose if the Shingrix diluent is inadvertently
administered alone

  

 

 

 

 

 

HELP! “We inadvertently gave a child only the
DTaP-IPV component of Pentacel, not realizing
that this component was intended ta

reconstitute the Hib component. Does this count as a

valid dose of DTaP and IPV? Can we mix the unused Hib

component with sterile water and give it separately?”

ANSWER

The DTaP-IPV component will count as valid doses of
DTaP and IPV vaccines, but take measures to prevent this
error in the future. You cannot mix the Hib component
with sterile water. ActHib must ONLY be reconstituted
with either the DTaP-IPV solution supplied with Pentacel,
or with a specific ActHib saline diluent. A

 

 

 

Immunization Action Coalition * (651) 647-3009 * www.immunize.org

HELP! “We mistakenly gave a patient the
diluent for Menveo (GSK) meningococcal
conjugate vaccine without adding it to the
powdered vaccine. What should we do now?”

 

ANSWER
Menveo's diluent contains the C, W-135, and Y serogroups, and the
freeze-dried powder contains serogroup A. Because the patient
received only the diluent, he or she is not protected against invasive
meningococcal disease caused by Neisseria meningitidis serogroup
A. Invasive disease with NV. meningitidis serogroup A is very rare in
the U.S,, but is more common in some other countries. If the
recipient (of the diluent only) is certain not to travel outside the
USS., then the dose does not need to be repeated. Otherwise, the
dose should be repeated with either correctly reconstituted
Menveo or with a dose of Menctra brand MenACWY.

 

 

 

 

www.immunize.org/catg.d/s8020.pdf « Item #88020 (12/19)
Giving the wrong vaccine will rarely cause
a serious problem, but.

 

Additional doses can lead to more vigorous local reactions ‘ bem oticperce
HELP! "Yesterday my 18-month-old’s pediatrician

Patient may be left unprotected against disease informed me that they made a mistake with her
Additional cost vaccines. They gave her two doses of Prevnar and
did not vaccinate for Hib. Will this harm my child?
Do I need to get a lawyer and attack this

* May cause loss of faith in provider or complaint to state incompetent practice? | am very concerned for my
board child and the impact it could have on her.”

* Inconvenience to patient/parent

   

 

Another administration error: If you give less than a full age-appropriate dose of

giving the wrong dose any vaccine, the dose is invalid. You should
revaccinate the person with the appropriate dose as

soon as feasible.* Exceptions are if a patient sneezes

HELP! “If an adult patient got a child's dose of hepatitis B after nasal spray vaccine or an infant regurgitates,

vaccine, should he be given an adult dose? If so, how

  

ne spits, or vomits during or after receiving oral
soon? : S
rotavirus vaccine.
HELP! “We had an incident recently where a 5-year-old + with Hop A, Hep 8 nd in luenze Vaccines, the pedatvc and adult
4 Gnruni , , products rete same ~ ust ferent amounts~<b this eros
presented for ‘catch up immunizations,’ but was given Sisooveredinmedately (sat cote day), ts pormisbe to adniter
an adult dose of hep A. We are wondering about side the other half and cout the two ae a full dose Buti the etor is

discovered later, the patient should be recalled and given a full ge:

effects or other possible issues. appropriate dose.

 

 

 

 

   

 

 

Another dosage error:
split or partial doses

If you give more than an age-appropriate dose of a
vaccine, count the dose as valid and notify the
patient/parent about the error. Using larger than
recommended dosages can be hazardous because of
excessive local or systemic concentrations of

antigens or other vaccine constituents.

© Split or partial (incomplete)
doses are NOT valid doses.
This includes situations where
the patient moves before the
injection is completed.
Exceptions to partial doses
+ LAW person sneeaes

+ Rotavirus infant reguraitates,
spits out, o¢ vomits

 

 

 

 

 

   

coc

 

Immunization Action Coalition * (651) 647-3009 * www.immunize.org www.immunize.org/catg.d/s8020.pdf Item #58020 (12/19)
 

Another dosage error:
combining vaccines

Vaccines should
NEVER be combined
in the same syringe
unless FDA approved
for this purpose.

 

 

coc

Another administration error:
using expired vaccine

 

coc

 

 

HELP! “A physician just called and gave a
child a dose of expired vaccine. lam
assuming the dose shauld be repeated.
Please advise.”

ANSWER

The dose should be repeated. If the expired dose isa live virus
vaccine, you should wait at least 4 weeks after the previous
expired} dose was given before repeating it. Ifthe expired dose is
not a live vaccine, the dose should be repeated as soon as possible.
(An exception to this rule is recombinant zoster vaccine, Shingrix;
‘you should wait 4 weeks to give a repeat dose after the invalid
dose.) If you prefer, you can perform serologic testing to check for
immunity for certain vaccinations (e.g., measles, mumps, rubella,
varicella, and hepatitis A). However, commercial serologic testing yam
might not always be sufficiently sensitive or standardized for ry

» detection of vaccine-induced immunity.

 

 

Another administration error:
Incorrect route, site, or needle size

 

 

 

 

  

 

 

Adapted COC Foon

 

 

 

HELP! “One of our nurses accidentally gave
Hep 6 SC rather than IM. Can you tell me
what we need to do?
ANSWER
Vaccines should always be given by the route recommended by the
manufacturer. However, ACIP and/or CDC have stated that the
action to be taken if an IM dose is inadvertently administered SC
varies by vaccine, as shown below.

ener
‘administered

i" i if madvertenty

     

Hepatitis abies, HPV inactivated
influenza

PCvAS, ib, DTaP Lette provider discretion
Hep A, MenACW,IPV, PPSU25, RZV | Yes. No need ta repeat the dose

No. Repeat the dose

 

 

 

Tap, Ta Men, Typhim V.JEVC_ | ACIPCDC has na recommendation

 

 

 

 

 

 

 

 

Immunization Action Coalition + (651) 647-9009 * www.immunize.org

Types of vaccination errors

Storage and handling

Administration

Scheduling

Documentation

 

 

 

www.immunize.org/catg.d/s8020.pdf © Item #58020 (12/19)
Scheduling errors: giving doses at too

young an age

* Giving the 1* dose of MMR or varicella before age 12,
months

© Giving the 4" dase of DTaP before age 12 months ar less
than 6 months after 3" dose

* Finishing infant’s hepB series before age 24 weeks

* Giving any vaccine (except hepatitis B) before 6 weeks of
age

 

 

HELP! “While registering her for kindergarten, it was
brought to my attention by the school RN that my
daughter’s initial MMR vaccine may not be valid.
She received this dose 25 days before her first
birthday. | do not want to re-administer a 3

vaccine if it is not necessary. It is painful and
excessive. What, if any, steps can I take to avoid
re-vaccinating my daughter?

 

 

Scheduling errors: giving doses without
the minimum spacing

* Giving 2”4 dose of hepatitis A vaccine less than 6 months
after the first dose

* Giving the hepatitis B vaccine series without at least 4 wks
between doses 1 and 2; 8 wks between doses 2 and 3; and
16 wks between doses 1 and 3

* Giving the 3 dose HPV vaccine series without at least 4 wks
between doses 1 and 2; 12 wks between doses 2 and 3;
and 24 wks between doses 1 and 3

* When the 2 dose HPV schedule is used (acceptable only
for those starting the series before the 15 birthday), the
minimum interval between the 2 doses is 5 months

 

 

 

CDC’s 4-day “grace period”

* Vaccine doses administered up to 4 days before the
minimum interval or age can be counted as valid

* This grace period should not be used when scheduling
future vaccination visits, or applied to the 28-day interval
between two different live parenteral vaccines not
administered at the same visit

* The grace period cannot be used for rabies vaccine

* Use of the grace period may conflict with state daycare or
school entry vaccination requirements

 

 

 

* Doses administered 5 ar more days before the minimum
age should be repeated on or after the patient reaches the
minimum age. If the vaccine is a live vaccine, waiting at
least 28 days from the invalid dose is recommended.

* ACIP does not require a minimum interval when an
inactivated vaccine is given before the minimum age. Once
the minimum age is reached, the repeat dose can be given
and can be counted.

« HOWEVER, some state immunization registries follow a
stricter rule, and, when a dose is given before the
minimum age, require that the next dose be given after
both the minimum age and interval.

 

 

   

Immunization Action Coalition * (651) 647-3009 * www.immunize.org

A dose administered 5 or more days earlier than the
recommended minimum interval between doses is
not valid and must be repeated. The repeat dose
should be spaced after the INVALID dose by the
recommended minimum interval.

 

 

www.immunize.org/catg.d/s8020.pdf « Item #58020 (12/19)

 

 
Aclinician’s best friend...

CDC's “Recommended and Minimum Ages and Intervals
Between Doses of Routinely Recommended Vaccines”
www.cdc.gov/vaccines/pubs/pinkbook/downloads/
appendices/A/age-interval-table.paf

Or check with your state registry about when the next
dose should be given!

 

  

 

 

Other scheduling errors

+ Giving rotavirus vaccine after age 8 months 0 days

+ Giving PPSV every S years

+ Giving PPSV and PCV at the same time

+ Not allowing 6 months between the next-to-last and last
doses of PV

+ Using Kinrix or Quadracel for the wrong dose or at the
wrong age

* Giving live vaccines not administered at the same visit
less than 4 weeks apart

 

 

 

HELP! “A client received an MMR vaccine at
one clinic, and 7 days later received varicella
vaccine at another Clinic. | assume the
varicella is not valid. What about the MMR?”

ANSWER

If two live virus vaccines are administered less than 4
weeks apart and not on the same day, the vaccine
given second should be considered invalid and
repeated. The repeat dose should be administered at
least 4 weeks after the invalid dose. Alternatively,

one can perform serologic testing to check for
immunity, but this option may be more costly
and/or may result ina false negative.

 

 

 

And the classic:

Re-starting a vaccine series because of
a longer-than-recommended interval

 

 

 

Immunization Action Coalition + (651) 647-3009 * www.immunize.org

IMPORTANT RULE:

Vaccine doses should not be administered at
intervals less than the recommended minimal
intervals or earlier than the minimal ages.

But there is not maximum interval!
{except for oral typhoid vaccine in some circumstances}

 

 

www.immunize.org/catg.d/s8020.pdf © Item #58020 (12/19)

 

 

 

 
Types of vaccination errors

Storage and handling

Administration

Scheduling

* Documentation

 

Types of documentation errors

Not providing a Vaccine Information Statement (VIS) every
time a vaccine is given

* Not using the most current VIS
Not knowing if written consent is required

Not recording all required information in the patient's
chart,

 

 

 

HELP! My 2-month-old was recently inoculated
at his pediatrician’s office. The day following the
immunizations my son spiked a high fever, and |
was extremely concerned. | called our local hospital and
found out that I should have been given a VIS sheet for
each of the inoculations that my child received. I did bring
this matter up with the pediatrician’s office, and | was told
by the office manager that she didn’t know of any law that
mandated they give information sheets out. My question
is, to wham do | report this incident? | no longer take my
child to their office, but | want them to start doing things
right.”
A minor side effect becomes a big problem
because the parent wasn't given a VIS...

 

 

 

How to ensure you are using the current VIS

* Check CDC's VIS web page
www. cde.gov/vaccines/hep/vis/index. html

* Check IAC’s VIS web page
www.immunize.org/vis

* Subscribe to JAC Express and be notified of any new and
revised VISs and translations every Wednesday
www.immunize.org/subscribe

 

 

 

 

 

HELP! “For a child, do we have the parent
sign each time we give a vaccine in a series,
or is it enough to have them sign for the first
one?

ANSWER

There is no federal law requiring written consent to

vaccines. VISs cover both benefits and risks associated

with vaccination, and they provide enough
information that anyone reading them should be
adequately informed. However, some states or
institutions have written informed consent laws.

Check with your state immunization program

and your institution.

 

 

 

Immunization Action Coalition * (651) 647-3009 * www.immunize.org

Required information to document

* Type of vaccine (e.g., MMR or Hib, NOT brand name)

* Vaccine manufacturer and lot number

* Date the vaccination was given

+ Name, office address, and title of the healthcare provider
administering the vaccine

* VIS edition date

* Date the VIS was given to the patient, parent, or guardian

* Use your state’s Immunization Information System
(registry)!

 

 

 

www.immunize.org/catg.d/s8020.pdf « Item #58020 (12/19)

10
‘cine riitirion Record
frchiderand Tere

  

Made a vaccination error?

The Institute for Safe Medication Practices (ISMP) has a
website to report vaccine errors — the Vaccine Error
Reporting Program (VERP).

VERP was created to allow healthcare professionals and
patients to report vaccine errors confidentially. By
collecting and quantifying information about these
errors, ISMP will be better able to advocate for changes
in vaccine names, labeling, or other appropriate
modifications that could reduce the likelihood of vaccine
errors in the future.
http://verp.ismp.org

 

 

In March 2015, VERP published an
excellent guide on avoiding vaccine errors:

 

Acute Care
|_ ISMPMi

  

Part Anlpls OFM cine rors Reporting Program VER

 

 

 

www. ismp.org/newsletters/acutecare/showarticle.aspxvid=104

IAC also has two comprehensive guides:

Don't Be Gulty of These Preventable Errors in Vaccine Administration!
‘seve immunize. orgicatg.d/p3823.pul

Don’t Be Gulty of These Preventable Errors in Vaccine Storage and Handling!
‘wwe amemuttize-org/caty 4 p3336. pu

 

 

 

Made a vaccination error? (cont.}

CDC recommends that healthcare professionals also
report vaccine errors to the Vaccine Adverse Events
Reporting System (VAERS). If an adverse event occurs
following a vaccine administration error, a report should
definitely be sent to VAERS.

Adverse events should be reported to VAERS regardless
of whether a healthcare professional thinks it's related to
the vaccine or not, as long as it follows administration of
a dose of vaccine

http://vaers.hhs.gov/index

 

 

 

 

How to avoid vaccine errors...
sew HELP!

HELP!

HELP!

HELP!

HELP!

HELP!

 

 

Immunization Action Coalition + (651) 647-3009 ¢ www.immunize.org

 

Educate yourself
* Read CDC's “Pink Book” cover to cover
www. cde.gov/vaccines/pubs/pinkbook/chapters. html

* Look for answers in the relevant ACIP recommendations
www.immunize.org/acip

* Read IAC’s “Ask the Experts” Q&As.
www.immunize.org/askexperts

* Subscribe to JAC Express for weekly updates
www.immunize.org/subscribe

 

 

 

www.immunize.org/catg.d/s8020.pdf © Item #58020 (12/19)

 

a
 

Educate yourself (cont.)
+ “Immunization Techniques” OVD
www.immunize.org/dvd
IAC’s resources related to:
+ Storage & handling
www. immunize.org/handouts/vaccine-storage-handling.asp
+ Vaccine administration
www.immunize.org/handouts/administering-vaccines.asp
* Vaccine recommendations, including scheduling
www. immunize.org/handouts/vaccine-recommendations.asp

+ Documentation megane
www.immunize.org/handouts/document-vaccines.asp

 

Immunization Action Coalition + (651) 647-3009 * www.immunize.org

Need more help?

* Contact CDC's experts: cde.gov/cde-info
* Contact your vaccine rep or call the manufacturer

* Call your state immunization coordinator
(contact information can be found at
ywaww.immunize.org/coordinators}

A

 

www.immunize.org/catg.d/s8020.pdf © Item #58020 (12/19)

12
