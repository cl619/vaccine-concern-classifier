Please tell us about meningococcal disease.
Meningococcal disease is a bacterial infection caused by Neisseria meningitidis. Meningococcal disease usually presents clinically as meningitis (about 50% of cases), bacteremia (30% of cases), or bacteremic pneumonia (15% of cases). N. meningitidis colonizes mucosal surfaces of the nasopharynx and is transmitted through direct contact with large-droplet respiratory tract secretions from patients or asymptomatic carriers. Meningococcal disease can be severe. The overall case-fatality ratio in the U.S. is 15%, and 10%20% of survivors have long-term sequelae such as neurologic disability, limb or digit loss, and hearing loss.
N. meningitidis is classified into 12 serogroups based on characteristics of the polysaccharide capsule. Most invasive disease (such as meningitis and sepsis) is caused by serogroups A, B, C, W, X and Y. The relative importance of serogroups depends on geographic location and other factors such as age. Serogroups B and C are the most frequent causes of disease in the U.S., accounting for 42% and 26% of cases with known serogroup, respectively, during 20152018. Serogroups W, Y, and nongroupable strains each caused 9%14% of cases during that period. Serogroup A is rare in the U.S. Historically, serogroup A was common in the meningitis belt of sub-Saharan Africa, but after the implementation of a meningococcal serogroup A conjugate vaccine campaign, serogroup A disease has been nearly eliminated in the meningitis belt.
Nasopharyngeal carriage rates are highest in adolescents and young adults who serve as reservoirs for transmission of N. meningitidis.
How common is meningococcal disease?
The incidence of meningococcal disease has declined steadily in the U.S. since a peak of reported disease in the late 1990s. Even before routine use of a meningococcal conjugate vaccine (MenACWY) in adolescents was recommended in 2005, the overall annual incidence of meningococcal disease had decreased 64%, from 1.1 cases per 100,000 population in 1996 to 0.4 cases per 100,000 population in 2005. In 2018, the rate of meningococcal disease in the U.S. reached a historic low of 0.1 cases per 100,000 population. Incidence of disease caused by serogroup B, a serogroup not included in the routinely recommended MenACWY vaccine, also has declined for reasons that are not known.
During 20152018, an estimated 360 cases of meningococcal disease occurred annually in the United States, representing an average annual incidence of 0.11 cases per 100,000 population. Of those with known serogroup in 2018 (N=302), 39% were serogroup B and 51% were serogroups C, Y, or W-135. The incidence of disease is highest in infants under 1 year, children age 1 year, and adolescents age 1620 years.
What groups are at increased risk for meningococcal disease?
In addition to risk based on age, non-specific risk factors for serogroups A, C, W and Y include having a previous viral infection, living in a crowded household, having an underlying chronic illness, and being exposed to cigarette smoke (either directly or second-hand).
The following groups are at increased risk for all meningococcal serogroups:
•
People with persistent (genetic) complement component deficiencies (a type of immune system disorder)
•
People who use complement inhibitors such as eculizumab (Soliris, Alexion Pharmaceuticals) and ravulizumab (Ultomiris, Alexion Pharmaceuticals) for treatment of atypical hemolytic uremic syndrome or paroxysmal nocturnal hemoglobinuria
•
People with anatomic or functional asplenia
•
Microbiologists routinely exposed to meningococcal isolates in a laboratory
•
People at increased risk during an outbreak of meningococcal disease
•
Military recruits
•
College students
Certain groups are at increased risk of serogroups A, C, W and Y, but not serogroup B:
•
People living with HIV
•
Men who have sex with men (MSM)
•
Travelers to countries where meningococcal disease is endemic or hyperendemic, such as the meningitis belt of sub-Saharan Africa
What meningococcal vaccines are available in the United States?
The vaccines for meningococcal serogroups A, C, W, and Y (MenACWY; Menactra, Sanofi Pasteur; Menveo, GlaxoSmithKline [GSK]; MenQuadfi, Sanofi Pasteur) contain meningococcal conjugate in which the surface polysaccharide is chemically bonded ("conjugated") to a protein to produce a robust immune response to the polysaccharide. Although each of the 3 MenACWY vaccine products uses a different protein conjugate, the products are considered interchangeable; the same vaccine product is recommended, but not required, for all doses.
A discontinued meningococcal polysaccharide vaccine (MPSV4, Menomune, Sanofi Pasteur) was available in the United States until all doses expired in September 2017. With rare exception, it was not interchangeable with MenACWY conjugate vaccines.
Since late 2014, vaccines have become available that offer protection from meningococcal serogroup B disease (MenB; Bexsero, GSK; Trumenba, Pfizer). These vaccines are composed of proteins found on the surface of the bacteria. These vaccine products are not interchangeable; the same vaccine product is required for all doses.
MenACWY vaccines provide no protection against serogroup B disease, and meningococcal serogroup B vaccines (MenB) provide no protection against serogroup A, C, W, or Y disease. For protection against all 5 serogroups of meningococcus, it is necessary to receive both MenACWY and MenB.
Trade Name
Type of Vaccine
Serogroups
Year Licensed
Approved Ages
Menactra
Conjugate
A, C, W, Y
2005
9 mos.55 years*
Menveo
Conjugate
A, C, W, Y
2010
2 mos.55 years*
MenQuadfi
Conjugate
A, C, W, Y
2020
2 years and older
Trumenba
Protein
B
2014
1025 years*
Bexsero
Protein
B
2015
1025 years*
Where can I find the most current meningococcal vaccine recommendations?
Who is recommended to be vaccinated against meningococcal ACWY disease?
MenACWY is recommended for these groups:
Routine vaccination of all children and teens, age 11 through 18 years: a single dose at age 11 or 12 years with a booster dose at age 16 years
Routine vaccination of people age 2 months or older at increased risk for meningococcal disease (the primary dosing schedule and booster dose interval varies by age and indication):
•
People with functional or anatomic asplenia
•
People who have persistent complement component deficiency (an immune system disorder) or who take a complement inhibitor (eculizumab [Soliris] or ravulizumab [Ultomiris])
•
People who have HIV infection
•
People who are at risk during an outbreak caused by a vaccine serogroup
•
People age 2 months and older who reside in or travel to certain countries in sub-Saharan Africa as well as to other countries for which meningococcal vaccine is recommended (e.g., travel to Mecca, Saudi Arabia, for the annual Hajj)
•
Microbiologists who work with meningococcus bacterial isolates in a laboratory
•
First-year college students living in residence halls who are unvaccinated or undervaccinated; these students should receive a dose if they have not had a dose since turning 16 or if it has been more than 5 years since their previous dose
What is the schedule for MenACWY vaccine?
All adolescents should receive a dose of MenACWY at 11 or 12 years of age. A second (booster) dose is recommended at 16 years of age. Adolescents who receive their first dose at age 13 through 15 years should receive a booster dose at age 16 years. The minimum interval between MenACWY doses is 8 weeks. Adolescents who receive a first dose after their 16th birthday do not need a booster dose unless they become at increased risk for meningococcal disease. Colleges may not consider a second dose given even a few days before age 16 years as valid, so keep that in mind when scheduling patients. People 19 through 21 years of age are not recommended routinely to receive MenACWY. However, MenACWY may be administered to people age 19 through 21 years as catch-up vaccination for those who have not received a dose after their 16th birthday.
If a child without any high risk conditions received a dose of MenACWY (Menactra, MenQuadfi or Menveo) vaccine at age 10 years does the child need to be revaccinated at age 1112 years?
No. ACIP considers a dose of MenACWY given to a 10-year-old child to be valid for the first dose in the adolescent series. Doses given before age 10 years should not be counted. The child should receive the second (booster) dose at age 16 years as usual.
Should college students be vaccinated against meningococcal ACWY disease?
First-year college students living in residence halls should be vaccinated against meningococcal ACWY disease. Before enrollment, administer a dose of MenACWY vaccine to those previously unvaccinated, to those who have not had a dose of MenACWY since turning 16, and to those whose most recent MenACWY dose (given after turning 16) was not given within the past 5 years. Some schools, colleges, and universities have policies requiring vaccination against meningococcal disease as a condition of enrollment.
Several healthy adult college students from outside the U.S. (ages 24 years and older) presented to our clinic. They will be living in a residence hall. None have a record of having received MenACWY. Should the receive a dose of MenACWY now?
Yes. One dose of MenACWY vaccine is recommended for all first year college students who are or will be living in a residence hall if they are previously unvaccinated, have not received a dose of MenACWY since turning 16, or if their most recent dose (given after turning 16) was not given within the past 5 years.
We run immunization clinics at the local jail, which has a living arrangement comparable to a college residential hall. In this setting, would you recommend vaccinating incarcerated individuals as is recommended for people living in a college dormitory?
ACIP does not identify incarceration as an indication for meningococcal vaccination. Providers are always free to use their clinical judgment in situations not addressed by ACIP.
Are there recommendations for meningococcal ACWY vaccination for people who reside in homeless shelters or halfway houses? In addition, can you comment on general vaccination recommendations for people who reside in homeless shelters or halfway houses?
Residence in a homeless shelter or halfway house is considered a high-risk indication only for hepatitis A vaccination because of the increased risk of hepatitis A exposure and serious illness among people experiencing homelessness or living in temporary housing. In all other respects, recommendations for vaccinating adult residents would be the same as those for all adults on the ACIP adult immunization schedule. Residents with medical conditions identified on Table 2 of the schedule should be vaccinated according to that table.
Any residents 18 or younger should be vaccinated according to the catch-up recommendations on the ACIP child/teen immunization schedule. People age 19 through 21 years are not recommended routinely to receive MenACWY. MenACWY may be administered through age 21 years as a catch-up vaccination for those who have not received a dose after their 16th birthday.
Our patient is starting college with no documented doses of meningococcal ACWY vaccine and has had titers drawn. The lab test was positive for A, C, W, and Y. Lab reference values show >2.1 as "suggestive of protection." Can we accept this titer in lieu of documented MenACWY vaccine doses?
No. There are no acceptable serologic titers that can be used as evidence of protection against meningococcal A, C, W, and Y disease. In addition, the immunologic studies used for licensing purposes (serum bactericidal assay, SBA) are likely different from the serologic titers obtained at a doctor's office (IgG antibody, for example). It is not clear what sort of testing is shown in the results you sent. However, even if SBA results are available, they cannot be used to assess whether there is a level of protection at the individual level.
Can you provide a comprehensive overview of the MenACWY recommendations, including those for vaccinating younger children and older adults who have risk factors?
Menveo (MenACWY-CRM) is approved by the FDA for use in children as young as 2 months of age. What is the ACIP recommendation for use of this vaccine?
I have a 3-month-old patient whose family will be doing mission work in sub-Saharan Africa. They are leaving as soon as the child is 6 months old. We gave her the first dose of Menveo brand MenACWY vaccine today. I know the usual Menveo schedule for an infant is 2, 4, 6, and 12 months. If we maintain usual spacing, she will only get 1 more dose before she leaves. Can we compress the schedule so she can get 2 more doses prior to travel?
The meningococcal ACIP recommendations don't clearly state a minimum interval for MenACWY in this situation. However, the minimum interval for a pediatric MenACWY schedule would presumably be 4 weeks like for other pediatric vaccines on a 2-4-6 schedule. You should try to give a third dose before travel begins.
If a healthy child received MenACWY or meningococcal polysaccharide (MPSV4) vaccination prior to international travel at age 9 years, will two additional doses of MenACWY be needed?
Yes. Doses of any quadrivalent meningococcal vaccine given before 10 years of age should not be counted as part of the adolescent MenACWY series. If a child received a dose of either MPSV4 or MenACWY before age 10 years, they should receive a dose of MenACWY at 11 or 12 years and a booster dose at age 16.
If someone received MenACWY vaccine at age 10 years and a dose of MenACWY before the 16th birthday, will they still need a booster dose at age 16?
Yes, they should receive a booster dose at age 16. A booster dose of MenACWY is recommended at age 16 years even if 2 (or more) doses of MenACWY vaccine were received before age 16 years. First-year college students living in a residence hall who have not received a dose of MenACWY on or after age 16 years, should also be vaccinated.
ACIP recommends that adolescents who receive the first dose of MenACWY at age 13 through 15 years receive a one-time booster dose at age 16 through 18 years. Given how hard it is to get teens into a medical office, is it okay to give the doses close together if the opportunity arises or should we try to space it out as far as possible (age 18)?
If the first dose is given at age 13 through 15 years, you can give the booster dose as early as age 16 years, with a minimum interval of 8 weeks from the previous dose. So even if the patient was vaccinated at age 15 years 11 months, you could wait at least 8 weeks and then give the booster at age 16 years 1 month (or later).
The ACIP recommendations advise using MenACWY in certain adults older than age 55 years. Please provide details of this recommendation. Two of the MenACWY vaccines (Menactra and Menveo) are approved for adults through age 55 years. MenQuadfi was approved in 2020 for ages 2 years and older. If MenACWY is indicated for a person older than age 55 and you do not have MenQuadfi, use the MenACWY product available.
I administer a lot of travel vaccine doses. What options do I have to give MenACWY to travelers age 56 years and older?
As of 2020, there are three options for MenACWY vaccination. In 2020, MenQuadfi (Sanofi Pasteur) was approved for use in all people ages 2 years and older. If MenQuadfi is not available and vaccination is needed, you may administer Menactra or Menveo.
Are the three MenACWY vaccines interchangeable?
Menactra (MenACWY-D) is not approved for children younger than 9 months so only Menveo (MenACWY-CRM) should be used for children age 2 through 8 months. MenQuadfi (MenACWY-TT) is not approved for children younger than age 2 years. From age 2 years and up the vaccines are interchangeable.
Which people age 2 years and older are recommended to receive a 2-dose primary series of MenACWY?
For people who are age 2 years or older, a 2-dose series of MenACWY, spaced 812 weeks apart, is recommended if they have functional or anatomic asplenia, HIV infection, persistent complement component deficiency (an immune disorder including C3, C5C9, properdin, factor H, and factor D deficiency), or if they take a complement inhibitor (eculizumab [Soliris] or ravulizumab [Ultomiris]). People with these high-risk medical conditions also need booster doses of MenACWY (see Booster Doses section below).
Which children should be vaccinated before the routine recommended age (1112 years)?
ACIP recommends meningococcal vaccination only for high-risk children younger than 11 years. ACIP defines high-risk children age 2 months and older as (1) those with persistent complement component deficiency (an immune system disorder) or who take a complement inhibitor (including eculizumab [Soliris] or ravulizumab [Ultomiris]), (2) those with functional or anatomic asplenia, (3) those with HIV infection, (4) those traveling to or residing in an area of the world where meningococcal disease is hyperendemic or epidemic or (5) those identified by public health officials as being at risk during a community outbreak attributable to a vaccine serogroup. Menveo (MenACWY-CRM) is approved for children age 2 months and older. Menactra (MenACWY-D) is approved for children age 9 months and older. MenQuadfi (MenACWY-TT) is approved for children age 2 years and older.
For children with functional or anatomic asplenia, Menactra should not be administered until at least 4 weeks after the pneumococcal conjugate vaccine (PCV13, Prevnar13, Pfizer) vaccination series is completed. Children at increased risk for meningococcal disease should receive booster doses as long as they remain at increased risk (see Booster Doses section below).
Why delay meningococcal vaccination with Menactra (MenACWY-D) for infants with HIV or functional or anatomic asplenia until the pneumococcal conjugate vaccine series is completed?
In addition to being at increased risk for meningococcal disease, children with HIV infection or functional or anatomic asplenia are at high risk for invasive disease caused by Streptococcus pneumoniae, which is more common than meningococcal disease. Data show that the Menactra may interfere with the immunologic response to PCV13 if these two vaccines are given too close together. So ACIP recommends that Menactra not be administered to children with these conditions before age 2 years to avoid interference with the response to PCV13. If Menactra is used in people of any age with these conditions, do not administer it until at least 4 weeks after completion of the PCV13 series. Menveo (MenACWY-CRM) and MenQuadfi (MenACWY-TT) do not affect the immune response to pneumococcal vaccine and can be given at any time before or after PCV13, although MenQuadfi is not licensed for use in children younger than age 2 years.
Can we vaccinate a 2-year-old boy with sickle cell disease against meningococcal disease if he has not completed a series of PCV13?
Possibly. If you are going to give him Menactra (MenACWY-D), you need to wait at least 4 weeks after he completes the PCV13 series before giving him the Menactra. There is no similar space consideration if Menveo (MenACWY-CRM) or MenQuadfi (MenACWY-TT) is used; these brands may be given simultaneously with PCV13 or at any interval before or after receipt of PCV13.
Adults who are asplenic need PCV13 and MenACWY. Does the recommendation to separate PCV13 and Menactra (MenACWY-D) apply to adults as well as children?
Yes. If Menactra (MenACWY-D) is being used, you should space it 4 weeks after PCV13. With both asplenic children and asplenic adults, if less than four weeks separate Menactra and PCV13 (in either order), the dose of PCV13 should be repeated four weeks after whichever vaccine was administered second.
Menveo (MenACWY-CRM) and MenQuadfi (MenACWY-TT) can be administered at any time before, simultaneous with, or after PCV13.
Do any of the bacterial vaccines that are recommended for people with functional or anatomic asplenia need to be given before splenectomy? Do the doses count if they are given during the 2 weeks prior to surgery?
PCV13, Haemophilus influenzae type b vaccine, MenACWY, and meningococcal B vaccine should be given 14 days before splenectomy, if possible. Doses given during the 14 days before surgery can be counted as valid. If the doses cannot be given prior to the splenectomy, they should be given as soon as the patient's condition has stabilized after surgery. Pneumococcal polysaccharide vaccine should be administered 8 weeks after the dose of PCV13 for people 2 years of age and older.
I have a pediatric patient who has functional asplenia. I gave her a dose of Menactra (MenACWY-D) when she was 3 years old. Do I need to give her a booster at some time?
Because she has functional asplenia, she is due for the second dose of the primary series (assuming 8 weeks have passed since the first primary series dose). Because she has a high-risk medical condition she will need periodic booster doses. If she is younger than age 7 years when she receives the second dose of her primary series, she should receive her first booster dose 3 years after completing the primary series. She should then receive a booster dose every five years thereafter. If she is age 7 years or older when she receives the second primary dose she should receive her first booster dose 5 years after the completing the primary series and every five years thereafter.
We have a 68-year-old who has been asplenic since 2009. She had one dose of meningococcal polysaccharide vaccine (MPSV4) in 2009, but no subsequent dose. She is now due for a booster. Should she receive 2 doses of MenACWY, 2 months apart, to catch up, or just one dose?
This situation is not addressed in the ACIP guidelines for meningococcal conjugate vaccine. It is the CDC meningococcal subject matter expert's opinion that this patient should receive 2 doses of MenACWY separated by at least 8 weeks, followed by a booster dose of MenACWY every 5 years thereafter. The concern is that having had only MPSV4 (Menomune, Sanofi Pasteur, discontinued in 2017) previously, she may not have an adequate booster response to a single dose of MenACWY.
I have a patient with paroxysmal nocturnal hemoglobinuria who is being treated with Soliris (eculizumab). Should he receive meningococcal vaccine?
Eculizumab (Soliris) and the related long-acting compound, ravulizumab (Ultomiris) bind to C5 and inhibit the terminal complement pathway. People with persistent complement component deficiency due to an immune system disorder or use of a complement inhibitor are at increased risk for meningococcal disease even if fully vaccinated. This patient should be given a series of MenACWY vaccine, MenACWY (2 doses separated by at least 8 weeks) and a 2- or 3-dose series (depending on brand) of MenB vaccine. The patient should receive regular booster doses of MenACWY and MenB as long as he remains at risk: a booster dose of MenACWY every 5 years and a booster dose of MenB one year after completion of the primary series, followed by a booster dose of MenB every 23 years thereafter.
Because patients treated with complement inhibitors can develop invasive meningococcal disease despite vaccination, clinicians using Soliris or Ultomiris also may consider antimicrobial prophylaxis for the duration of complement inhibitor therapy.
We have a 10-year-old getting renal dialysis. The nephrologist will be starting her on ravulizumab (Ultomiris), which interferes with C5 complement. If we administer MenACWY and pneumococcal polysaccharide vaccine (PPSV23) now, and then give her PCV13 in 8 weeks, will the PCV13 interfere with the efficacy of the PPSV23 or the MenACWY?
Recommendations to separate MenACWY and PCV13 only apply to one of the three MenACWY vaccines, Menactra (MenACWY-D), and also only apply to individuals with functional or anatomic asplenia or HIV infection. So the best schedule is to give MenACWY (any brand) simultaneously with PCV13, and then PPSV23 in eight weeks. ACIP recommends giving PCV13 before PPSV23 in order to maximize the immune response from PCV13. PPSV23 may blunt the immune response to PCV13 if PCV13 is given after PPSV23, although in children there is a smaller effect than in adults. A 10 year-old with persistent complement component deficiency also should receive a 2- or 3-dose series (depending on brand) of meningococcal B vaccine.
As long as the child remains at high risk of meningococcal disease due to complement inhibitor use, booster doses of both MenACWY and MenB are recommended. A MenACWY booster dose should be given every 5 years and a MenB booster dose should be given one year after the completion of the primary series, followed by a booster dose every 23 years thereafter.
Are people who are HIV-positive at increased risk for meningococcal disease?
People age 2 years and older with HIV infection who have not been previously vaccinated should receive a 2-dose primary series of MenACWY (doses separated by at least 8 weeks). People with HIV infection who have previously received one dose of MenACWY should receive a second dose at the earliest opportunity (at least 8 weeks after the previous dose) and then receive booster doses at the appropriate intervals (see Booster Doses below). ACIP does not recommend routine meningococcal serogroup B vaccination of people with HIV infection.
I have an otherwise healthy 26-year-old patient with HIV infection who received one dose of MenACWY three years ago. Should he receive one or two doses now? Will he need booster doses later?
It is not necessary to restart the MenACWY series. Give the person one dose of MenACWY vaccine now. This dose represents a delayed second dose in the primary series (a 2-dose primary series recommended for people with HIV infection). The patient will subsequently need booster doses every 5 years.
I have a 24-month-old patient with HIV infection and I want to use Menactra (MenACWY-D) because this is the only vaccine we have available in our clinic. However, this child received DTaP vaccine yesterday at another clinic. Can I administer MenACWY-D?
If Menactra (MenACWY-D) is to be administered to a child at increased risk for meningococcal disease, including children who have HIV infection, Menactra should be given either before, at the same visit, or at least 6 months after DTaP. This is because data suggest a reduced response to the Menactra if given within a month after DTaP. Menactra may be used earlier than 6 months after DTaP if it is the only available option and vaccination is necessary due to travel to an area with epidemic or hyperendemic meningococcal disease. Menveo (MenACWY-CRM) and MenQuadfi (MenACWY-TT) vaccines may be given at any time before or after DTaP.
I have a 24-month-old patient with a complement component deficiency who received a dose of DTaP at 23 months of age and then received a dose of Menactra (MenACWY-D) two weeks later. Do I need to repeat the dose of Menactra?
No. Even though ACIP recommends that Menactra (MenACWY-D) should be given either before, at the same visit, or at least 6 months after DTaP, there is no evidence to support repeating the dose of Menactra. A child with a complement component deficiency should still receive a second dose of MenACWY vaccine at least 8 weeks after the first dose. In this case, if the 2nd dose also will be Menactra, it should wait until the child is 29 months old (6 months after the dose of DTaP).
Does the recommendation for separation of DTaP and Menactra (MenACWY-D) also apply to children with functional or anatomic asplenia?
Yes. The recommendation about spacing of DTaP and Menactra (MenACWY-D) applies to all children younger than 7 years with a high-risk condition for meningococcal disease, including travelers. Menactra may be used earlier than 6 months after DTaP if it is the only available option and vaccination is necessary due to travel to an area with epidemic or hyperendemic meningococcal disease. Menveo (MenACWY-CRM) and MenQuadfi (MenACWY-TT) may be given at any time before or after DTaP.
A 32-year-old patient with ulcerative colitis is taking high-dose immunosuppressive medications (6-mercaptopurine). Should he receive meningococcal vaccine?
There is no specific indication for meningococcal vaccine in this patient. He is older than 21 years, and the risk–based recommendations are restricted to specific forms of altered immunocompetence (persistent complement component deficiency, functional or anatomic asplenia, use of eculizumab [Soliris] or ravulizumab [Ultomiris]) and HIV infection) and do not include other forms of altered immunocompetence.
Should I recommend MenACWY vaccine for a nonsmoker living in a crowded household of smokers?
Although second-hand smoke and other environmental conditions have been identified as risk factors for meningococcal disease, ACIP does not include them as indications for MenACWY vaccination. Providers may always use their clinical judgment in situations not addressed by ACIP.
Should all adolescents receive a routine booster dose of MenACWY?
ACIP recommends adolescents age 11 or 12 years be routinely vaccinated with MenACWY and receive a booster dose at age 16 years. Adolescents who receive the first dose at age 13 through 15 years should receive a one-time booster dose, preferably at age 16 through 18 years, just before the peak in incidence of meningococcal disease among adolescents occurs. Teens who receive their first dose of MenACWY at or after age 16 years do not need a booster dose, as long as they have no additional risk factors.
Why does ACIP recommend a routine booster dose of MenACWY for adolescents at age 16 years?
In 2005, ACIP recommended routine MenACWY vaccination for all adolescents at age 11 or 12 years to protect them from meningococcal disease as older teens. The peak age for meningococcal disease is 16 through 21 years. Subsequent studies indicated that the protection provided by MenACWY wanes within 5 years following vaccination. For this reason, in 2010, ACIP recommended a MenACWY booster dose to provide continuing protection during the age of increased meningococcal incidence.
Which previously vaccinated college students need a dose of MenACWY?
A booster dose should be given to first-year college students, regardless of age, who are or will be living in a residence hall if the previous dose was given before the age of 16 years or if their most recent dose (given after the 16th birthday) was not given within the past 5 years.
If someone received MPSV4 or MenACWY at age 9 years, will two additional doses of MenACWY be needed?
Yes. Doses of quadrivalent meningococcal vaccine (either MPSV4 or MenACWY) given before 10 years of age should not be counted as part of the series. If a child received a dose of either MPSV4 (Menomune, a meningococcal polysaccharide vaccine no longer available in the United States) or MenACWY before age 10 years, they should receive a dose of MenACWY at 11 or 12 years and a booster dose at age 16 years. A dose of MenACWY administered at age 10 may count as the first adolescent dose normally given at 11 or 12.
Which people with risk factors should receive booster doses beyond the routinely recommended adolescent doses of MenACWY?
Children at continued high risk who received the last dose of the primary series of MenACWY before age 7 years should receive the next dose 3 years after the most recent dose, then every 5 years as long as risk remains. People at continued high risk who received the last dose of the primary series at age 7 years or older should receive the next dose 5 years after the most recent dose then every 5 years as long as risk remains. Two of the 3 MenACWY brands are licensed through age 55 years; however, if MenQuadfi (MenACWY-TT, licensed for use at age 2 years and older) is unavailable for an adult age 56 years or older, you may use the available MenACWY product.
Should people with continued high risk of meningococcal disease receive additional doses of meningococcal vaccine beyond the 3- or 5-year booster?
Yes, people should receive additional booster doses (every 5 years) if they continue to be at highest risk for meningococcal infection.
If a child with a high-risk condition receives MenACWY at age 9 years (and a second primary dose 8 weeks later), should they receive a booster dose at age 14 years (5 years after the primary series), or should they receive a dose at age 16 years as recommended in the routine schedule?
The MenACWY booster dose should be given at 14 years (5 years after the primary series) and every 5 years thereafter. The every 5-year booster dose schedule for people with high-risk conditions takes precedence over the routine adolescent schedule.
What do you do if an adult patient is in a high-risk situation for meningococcal disease (for example travel to sub-Saharan Africa) and doesn't know whether they received MenACWY or MPSV4 in the past. Should we vaccinate them?
If the person cannot provide written documentation of the previous vaccination you should assume they are unvaccinated and vaccinate accordingly.
By what route should meningococcal vaccines be administered?
All meningococcal conjugate vaccines should be administered by the intramuscular route. Meningococcal serogroup B vaccine is given by the intramuscular route.
We mistakenly gave a patient the diluent for Menveo (MenACWY-CRM) without adding it to the powdered vaccine. Since vaccine antigen is present in the diluent as well as in the powder, what should we do now?
The liquid vaccine component (the diluent) of Menveo contains the C, W-135, and Y serogroups, and the lyophilized vaccine component (the freeze-dried powder) contains serogroup A. Because the patient received only the diluent, he or she is not protected against invasive meningococcal disease caused by N. meningitidis serogroup A.
Invasive disease with N. meningitidis serogroup A is very rare in the United States, but is more common in some other countries. If the recipient (of the C-Y-135 "diluent" only) is certain not to travel outside the United States then the dose does not need to be repeated. However, if the recipient plans to travel outside the United States the dose should be repeated with either correctly reconstituted Menveo, or with a dose of another brand of MenACWY. There is no minimum interval between the incorrect dose and the repeat dose.
Can MenACWY and MenB vaccines be given at the same visit?
Yes. MenACWY and MenB vaccines can be given at the same visit or at any time before or after the other.
What adverse events are expected after receiving MenACWY?
In all three brands of MenACWY, the most common adverse event were injection site pain, swelling or redness. Other reported symptoms included malaise and headache.
Is MenACWY included in the National Vaccine Injury Compensation Program?
What are the contraindications and precautions for MenACWY?
As with all vaccines, a severe allergic reaction (for example, anaphylaxis) to a vaccine component or to a prior dose is a contraindication to further doses of that vaccine. A moderate or severe acute illness is a precaution; vaccination should be deferred until the person's condition has improved. Because MenACWY is an inactivated vaccine, it can be administered to people who are immunosuppressed as a result of disease or medications; however, response to the vaccine might be less than optimal.
Can a pregnant woman receive MenACWY vaccine?
Yes. No safety concerns associated with vaccination have been identified in mothers vaccinated during pregnancy or their infants.
I understand that a prior history of Guillain-Barré syndrome (GBS) is no longer a precaution for giving meningococcal conjugate vaccine. Please tell me more about this.
A history of GBS had previously been a precaution for Menactra (MenACWY-D). Findings from two studies that examined more than 2 million doses of Menactra given since 2005 showed no evidence of an increased risk of GBS. Consequently, ACIP recommended in 2010 to remove the precaution for use of Menactra in people with a history of GBS. This precaution did not apply to other meningococcal vaccines.
What is the storage requirement for MenACWY?
Store any brand of MenACWY at refrigerator temperature, between 2° and 8°C (between 36° and 46°F). The vaccine must not be frozen. Vaccine that has been frozen or exposed to freezing temperature should not be used. Do not use after the expiration date.
