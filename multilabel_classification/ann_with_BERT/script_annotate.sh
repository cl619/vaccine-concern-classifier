#!/bin/bash
#SBATCH -p compsci-gpu
#SBATCH --gres=gpu:1
#SBATCH -n 1
#SBATCH --mem=8G
#SBATCH -J scale_test
#SBATCH -o scale_test.out

python3 annotate.py