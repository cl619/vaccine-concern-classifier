#!/bin/bash
#SBATCH -p compsci-gpu
#SBATCH --gres=gpu:1
#SBATCH -n 1
#SBATCH --mem=8G
#SBATCH -J weighted
#SBATCH -o weighted_%A_%a.out
#SBATCH --array=1-6

params=(3 10 30 100 'inf' 'log1p')
param=${params[$SLURM_ARRAY_TASK_ID-1]}

python3 script_weighted.py $param