#!/bin/sh
#SBATCH -p compsci-gpu
#SBATCH --gres=gpu:1
#SBATCH -n 1
#SBATCH --mem=8G
#SBATCH -J baseline
#SBATCH -o baseline_%j.out

python3 script_baseline.py