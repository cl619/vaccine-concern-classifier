from vax_concerns_classifiers import MultilabelWeighted, CATEGORIES
import numpy as np
import pandas as pd
import os
import json
from sklearn.model_selection import train_test_split
import torch
import argparse

LOCAL_DIR = os.path.dirname(os.path.abspath(__file__))
TRAIN_PATH = os.path.join(LOCAL_DIR, '..', '..', '..', 'data', 'multi_label_train', 'gpt_ann_short.csv')

# load data
train_df = pd.read_csv(TRAIN_PATH)
train_df = train_df[train_df['relevant'] == 1]
train_texts = train_df["paragraph_content"].to_list()
gpt_anns = [json.loads(d.replace("'", '"')) for d in train_df['gpt_ann']]
train_labels = np.array([[float(d[cat]) for cat in CATEGORIES] for d in gpt_anns])

# split into training, cutoff tuning, and validation datasets
train_texts, tuning_texts, train_labels, tuning_labels = train_test_split(train_texts, train_labels, test_size=500, random_state=42)
train_texts, val_texts, train_labels, val_labels = train_test_split(train_texts, train_labels, test_size=0.1, random_state=42)

def clamp(max_weight):
    return lambda pos_weight: torch.clamp(pos_weight, max=max_weight)

def log1p():
    return lambda pos_weight: torch.log1p(pos_weight)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Weighted classifier')
    parser.add_argument('weight_scheme', type=str, help='Number for hard max_weight, inf for no scheme, and log1p for logarithmic weighting')
    args = parser.parse_args()
    weight_scheme = args.weight_scheme

    if weight_scheme == 'inf':
        weight_func = lambda x: x
    elif weight_scheme == 'log1p':
        weight_func = log1p()
    else:
        weight_func = clamp(int(weight_scheme))

    weighted = MultilabelWeighted()
    weighted.fit((train_texts, train_labels), 
                (tuning_texts, tuning_labels),
                (val_texts, val_labels),
                weight_func)
    weighted.save_model(LOCAL_DIR, f"weighted_{weight_scheme}")