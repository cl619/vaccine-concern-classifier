from vax_concerns_classifiers import MultilabelBaseline, CATEGORIES
import numpy as np
import pandas as pd
import os
import json
from sklearn.model_selection import train_test_split

LOCAL_DIR = os.path.dirname(os.path.abspath(__file__))
TRAIN_PATH = os.path.join(LOCAL_DIR, '..', '..', '..', 'data', 'multi_label_train', 'gpt_ann_short.csv')

# load data
train_df = pd.read_csv(TRAIN_PATH)
train_df = train_df[train_df['relevant'] == 1]
train_texts = train_df["paragraph_content"].to_list()
gpt_anns = [json.loads(d.replace("'", '"')) for d in train_df['gpt_ann']]
train_labels = np.array([[float(d[cat]) for cat in CATEGORIES] for d in gpt_anns])

# split into training, cutoff tuning, and validation datasets
train_texts, tuning_texts, train_labels, tuning_labels = train_test_split(train_texts, train_labels, test_size=500, random_state=42)
train_texts, val_texts, train_labels, val_labels = train_test_split(train_texts, train_labels, test_size=0.1, random_state=42)

baseline = MultilabelBaseline()
baseline.fit((train_texts, train_labels), 
             (tuning_texts, tuning_labels),
             (val_texts, val_labels))
baseline.save_model(LOCAL_DIR, "baseline")