import os
import pandas as pd
import numpy as np
from tqdm import tqdm

import torch
from torch import nn
from transformers import BertModel, BertTokenizer

DATA_FILE = '../../data/full_processed.csv'
TRAIN_DIR = '../../data/multi_label_train'
GPT_ANN_PATH = os.path.join(TRAIN_DIR, "gpt_ann.csv")

# initialize an empty annotation csv
def make_gpt_ann_csv(reset=False):
    if os.path.exists(GPT_ANN_PATH) and not reset:
        return
    
    df = pd.read_csv(DATA_FILE)
    df.drop('publisher_name', inplace=True, axis=1)
    LEN = len(df)

    df['relevant'] = [None] * LEN
    df['gpt_ann_txt'] = [None] * LEN
    df['gpt_ann_raw'] = [None] * LEN
    df['gpt_ann'] = [None] * LEN
    df.to_csv(GPT_ANN_PATH, index=False)

make_gpt_ann_csv()

# Set up parameters
bert_model_name = 'bert-base-uncased'
num_classes = 2
bert_path = "../../relevance_classifiers/bert_classifier/bert_classifier.pth"

class BERTClassifier(nn.Module):
    def __init__(self, bert_model_name, num_classes):
        super(BERTClassifier, self).__init__()
        self.bert = BertModel.from_pretrained(bert_model_name)
        self.dropout = nn.Dropout(0.1)
        self.fc = nn.Linear(self.bert.config.hidden_size, num_classes)

    def forward(self, input_ids, attention_mask):
        outputs = self.bert(input_ids=input_ids, attention_mask=attention_mask)
        pooled_output = outputs.pooler_output
        x = self.dropout(pooled_output)
        logits = self.fc(x)
        return logits

tokenizer = BertTokenizer.from_pretrained(bert_model_name)

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model = BERTClassifier(bert_model_name, num_classes).to(device)
model.load_state_dict(torch.load(bert_path))

def predict_relevance(text, model, tokenizer, device, max_length=128):
    model.eval()
    encoding = tokenizer(text, return_tensors='pt', max_length=max_length, padding='max_length', truncation=True)
    input_ids = encoding['input_ids'].to(device)
    attention_mask = encoding['attention_mask'].to(device)

    with torch.no_grad():
        outputs = model(input_ids=input_ids, attention_mask=attention_mask)
        _, preds = torch.max(outputs, dim=1)
        return preds.item()

df = pd.read_csv(GPT_ANN_PATH)
relevant = df['relevant'].to_list()
paragraphs = df['paragraph_content'].to_list()
for i in tqdm(range(len(df))):
    relevant[i] = predict_relevance(paragraphs[i], model, tokenizer, device)
df['relevant'] = relevant
df.to_csv(GPT_ANN_PATH, index=False)

