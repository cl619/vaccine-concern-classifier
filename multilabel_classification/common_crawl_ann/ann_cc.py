from vax_concerns_classifiers import MultilabelWeighted, CATEGORIES
import os
from tqdm import tqdm
import pandas as pd
from datasets import Dataset
from nltk.tokenize import sent_tokenize, word_tokenize
import glob

TOKENIZER_MAX_LENGTH = 128

def combine_tokens(tokens: list[str]):
    return ' '.join(tokens).replace(' ,', ',').replace(' .', '.').replace(' !', '!').replace(' ?', '?').replace(" '", "'").replace(" ’ ", "’").replace("“ ", "“").replace(" ”", "”").replace(" :", ":").replace("( ", "(").replace(" )", ")").replace("can not", "cannot").replace(" %","%").replace("do n't", "don't")

def chunkify(sentences: list[str]):
    ret = []
    currChunk = []
    for s in sentences:
        if len(currChunk) + len(s) <= TOKENIZER_MAX_LENGTH:
            currChunk.extend(s)
        elif len(s) <= TOKENIZER_MAX_LENGTH:
            ret.append(combine_tokens(currChunk))
            currChunk = s
        else:
            ret.append(combine_tokens(currChunk))
            ret.append(combine_tokens(s))
            currChunk = []
    if currChunk:
        ret.append(combine_tokens(currChunk))
    return ret

def parse_text(text: str) -> list[str]:
    ret = []
    for sample in text.split('\n'):
        tokenized_sentences = [word_tokenize(sent) for sent in sent_tokenize(sample)]
        ret.extend(chunkify(tokenized_sentences))
    return ret

LOCAL_DIR = os.path.dirname(os.path.abspath(__file__))
DATA_DIR = "/usr/xtmp/cl619/cc-24"

pattern = "*L*/labeled_samples_all/data*.arrow"
matching_files = glob.glob(os.path.join(DATA_DIR, pattern), recursive=True)

weighted = MultilabelWeighted()
weighted.load_model(os.path.join(LOCAL_DIR, '..', 'bert_classifier', 'scripts', 'weighted_log1p'), 'weighted_log1p')

for file in matching_files:
    print(f"Processing {file}")

    # pull out English samples
    dataset = Dataset.from_file(file)
    dataset = dataset.filter(lambda row: row['languages'] and row['languages']['languages'] and row['languages']['languages'][0]['code'] == 'en')

    # preprocess dataset into chunks
    preprocessed_dataset = {
        'record_id': [],
        'index': [],
        'text': [],
        'bert_ann': [],
    }
    for i, row in tqdm(enumerate(dataset), total=len(dataset)):
        chunked = parse_text(row['text'])
        preprocessed_dataset['record_id'].extend([row['record_id']] * len(chunked))
        preprocessed_dataset['index'].extend(range(len(chunked)))
        preprocessed_dataset['text'].extend(chunked)

    # make predictions on text
    labels = weighted.predict(preprocessed_dataset['text'])
    preprocessed_dataset['bert_ann'] = [{cat:classif for cat, classif in zip(CATEGORIES, l)} for l in labels]

    # save preprocessed dataset
    preprocessed_df = pd.DataFrame(preprocessed_dataset)
    parts = file.split(os.sep)
    new_file = os.path.join(parts[5], parts[7])[:-6] + "_preprocessed.csv"

    new_dir = os.path.dirname(new_file)
    os.makedirs(new_dir, exist_ok=True)

    preprocessed_df.to_csv(new_file, index=False)
    print(f"Saved to {new_file}")
