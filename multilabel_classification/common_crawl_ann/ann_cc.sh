#!/bin/bash
#SBATCH -p compsci-gpu
#SBATCH --gres=gpu:1
#SBATCH -n 1
#SBATCH --mem=8G
#SBATCH -J ann_cc
#SBATCH -o ann_cc.out

python3 ann_cc.py